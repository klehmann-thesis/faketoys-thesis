# Faketoys

Generate random variables and propagate them through equations. This code was developed to run pseudo-experiments of the Fake Factor Equation in high-energy physics, but is flexible and can be adapted.

## Getting Started

You have three options to set up the code (see below). Also take a look at the environment variables if you want to do more serious development. Once you're ready to go, follow the [short introduction](https://klehmann-hep.gitlab.io/faketoys/short_doc.html). Also feel free to glance at the rest of the [documentation](https://klehmann-hep.gitlab.io/faketoys/).

### Docker image with source code
Download a docker image that contains the source code and all dependencies. Then, run inside the container. For version `0.1.0`, for example, do this with the commands
```bash
docker pull kolehman/faketoys:0.1.0
docker run --rm -it kolehman/faketoys:0.1.0
```

### Docker image without source code
Download the git repository and the docker base image that contains all dependencies. Then, mount the git repository into the container, like
```bash
git clone git@gitlab.com:klehmann-hep/faketoys.git
docker pull kolehman/faketoys:base
docker run --rm -it -v /path/to/faketoys:/faketoys -w /faketoys kolehman/faketoys:base
```
Inside the container, run `source setup.sh` to tell python, where to find the code.

### Install python packages
Download the git repository like above. Then use `pip install` with `python3` to get all dependencies on your local system. You can find the commands and dependencies in the [Dockerfile](https://gitlab.com/klehmann-hep/faketoys-docker/-/blob/master/Dockerfile) in the related repository [faketoys-docker](https://gitlab.com/klehmann-hep/faketoys-docker). Afterwards, run `source setup.sh`.

## Environment variables
The following environment variables should be set for convenience. They ensure that the code can be executed from any directory and will produce the same result.
* `FAKEBASE`: Path to the top git directory. This variable is used when using the submission scripts for slurm to find the right directory. It is also used to resolve the location of config files.
* `FAKEOUTPUT`: All generated files are written into this directory. Also generated files that are used as input files (eg. prepared files in the compute step) are assumed to be here.

If you create a file called `setupLocal.sh`, it will automatically sourced during `setup.sh`.
