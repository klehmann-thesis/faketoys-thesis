from faketoys import utils, log
import xarray as xr

i = 0

smartBins_random = True
smartBins_nBins = 30
smartBins_nStddev = 5

randomHistLowX =  -10
randomHistHighX = 30
randomHistStepX = 0.5

pValueHistStepX = 0.02

preparedOutputFile = "prepared_minimalTest.pkl"
computedOutputFile = "computed_minimalTest.pkl"

compute_nRandom = 10

def getDataArray():
    bins = {}
    bins["FF_Nt"]      = [125]#, 150, 175]
    bins["FF_Ntr"]     = [75]#, 100, 125]
    bins["FF_Ntr_unc"] = [10]
    bins["FF_Nl"]      = [250]#, 300, 400]
    bins["FF_Nlr"]     = [50]#, 70, 90]
    bins["FF_Nlr_unc"] = [40]

    bins["Nl"]         = [16, 20]
    bins["Nlr"]        = [8, 12]
    bins["Nlr_unc"]    = [4, 8]

    dimensions = [x for x in bins.keys()]

    array = xr.DataArray(dims=tuple(dimensions), coords=bins)
    array = array.astype("object")
    utils.initializeArray(utils.getFFE_1l, array)

    return array
