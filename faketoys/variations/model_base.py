from faketoys import log

class VariationModelBase(log.Logger):
    '''Base class for variation models.

    To determine how a variable depends on a systematic variation, the
    method calculateVariation() can be used. It should at least take
    the random value drawn from the underlying distribution and can get
    additional keyword arguments. It returns the systematic variation.

    See examples in :py:mod:`~faketoys.variations.models`.
    '''

    def __init__(self, name=None):
        super().__init__(name=name)

    def calculateVariation(self, x, **kwargs):
        raise NotImplementedError("Classes inheriting from VariationModelBase must implement calculateVariation()")
    def _calculateVariationExample(self, x):
        '''Example for how to use :py:meth:`calculateVariation`.

        This method is only used internally to find out if the true
        value of the underlying distribution causes a variation of 0.
        This is a good consistency check. All variables needed by
        :py:meth:`calculateVariation` other than the random value `x`
        need to be specified.

        This method needs to be overwritten by a model, which needs more
        inputs than just the random variable `x`.
        '''
        return self.calculateVariation(x)
