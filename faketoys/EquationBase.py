import sys
from faketoys import log

from enum import Enum
class TrueMeasuredCategory(Enum):
    INVALID         = 0
    OVERALL         = 1
    TRUE            = 2
    MEASURED        = 3
    TRUEANDMEASURED = 4

class EquationBase():
    # This is a base class with useful methods for the matrix and fake factor equation

    def __init__(self, nLeptons):
        if not isinstance(nLeptons, int):
            self.log.error("nLeptons has to be an integer.")
        # number of leptons
        self.nLeptons = nLeptons
        # dimension of matrix (= number combinations)
        self.dimension = 2 ** self.nLeptons
        # higher number --> more messages printed
        self.debugLevel = 0
        return

    # todo: is this needed?
    def equalTo(self, compare):
        if not self.nLeptons == compare.nLeptons:
            return False
        elif not self.dimension == compare.dimension:
            return False
        else:
            return True

    def message(self, s, i = 0):
        if self.debugLevel >= i:
            print(i*" " + s)

    # When looping over combinations of leptons, a lepton index can be used.
    # This index is mapped onto a unique lepton combination.
    # If the user wants to specify a certain lepton combination, it is easiest
    # to use a notation like "ttl", "frf" for "tight, tight, loose" and
    # "fake, real, fake". The tight and loose categories are called measured
    # types, the real and fake categories are called true types.
    # Internally, both of these inputs are converted to strings of binary
    # numbers. The following methods help with this conversion.

    ### Basic conversions (index <--> binary, binary <--> types) ###

    # index --> binary number
    def indexToBinary(self, index):
        # Converts index into string of real (0) and fake (1) or tight (0) and loose (1)
        types = format(index, 'b')
        types = (self.nLeptons - len(types))*"0" + types
        return types

    # binary number --> index
    def binaryToIndex(self, types):
        return int(types, 2)

    # true types --> binary number
    def trueTypesToBinary(self, trueTypes):
        binary = trueTypes.replace("r", "0")
        binary = binary.replace("f", "1")
        return binary

    # binary number --> true types
    def binaryToTrueTypes(self, binary):
        trueTypes = binary.replace("0", "r")
        trueTypes = trueTypes.replace("1", "f")
        return trueTypes

    # measured types --> binary number
    def measuredTypesToBinary(self, measuredTypes):
        binary = measuredTypes.replace("t", "0")
        binary = binary.replace("l", "1")
        return binary

    # binary number --> measured types
    def binaryToMeasuredTypes(self, binary):
        measuredTypes = binary.replace("0", "t")
        measuredTypes = measuredTypes.replace("1", "l")
        return measuredTypes

    ### Sequential conversions (index <--> types) ###

    # index --> true types (real, fake)
    def indexToTrueTypes(self, index):
        binary = self.indexToBinary(index)
        trueTypes = self.binaryToTrueTypes(binary)
        return trueTypes

    # index --> measured types (tight, loose)
    def indexToMeasuredTypes(self, index):
        binary = self.indexToBinary(index)
        measuredTypes = self.binaryToMeasuredTypes(binary)
        return measuredTypes

    # true types --> index
    def trueTypesToIndex(self, trueTypes):
        binary = self.trueTypesToBinary(trueTypes)
        index = self.binaryToIndex(binary)
        return index

    # measured types --> index
    def measuredTypesToIndex(self, measuredTypes):
        binary = self.measuredTypesToBinary(measuredTypes)
        index = self.binaryToIndex(binary)
        return index

    ### Check if content of types looks fine ###

    # To be used as internal method. Returns true if types only contains
    # allowedCharacters. If allowEmpty, then an empty types string returns
    # true. varName is only used for printing an error message.
    def checkTypes(self, types, allowedCharacters, varName, allowEmpty):
        if allowEmpty and types == "":
            return True
        retval = True
        if not set(types).issubset(allowedCharacters):
            print("ERROR: " + varName + " '" + types + "' contains invalid characters (allowed are " + str(allowedCharacters) + ")")
            retval = False
        if not len(types) == self.nLeptons:
            print("ERROR: " + varName + " '" + types + "' does not have one character per lepton (" + str(self.nLeptons) + ")")
            retval = False
        return retval

    # wrapper around checkTypes
    def checkTrueTypes(self, trueTypes, allowEmpty = False):
        return self.checkTypes(trueTypes, set("rf"), "trueTypes", allowEmpty)

    # wrapper around checkTypes
    def checkMeasuredTypes(self, measuredTypes, allowEmpty = False):
        return self.checkTypes(measuredTypes, set("tl"), "measuredTypes", allowEmpty)

    # combine the two above methods
    def checkTrueAndMeasuredTypes(self, trueTypes, measuredTypes, allowEmpty = False):
        check1 = self.checkTrueTypes(trueTypes, allowEmpty = allowEmpty)
        check2 = self.checkMeasuredTypes(measuredTypes, allowEmpty = allowEmpty)
        return check1 and check2

    # test the various combinations and return an enum TrueMeasuredCategory
    def analyzeTrueAndMeasuredTypes(self, trueTypes, measuredTypes):
        if (not self.checkTrueAndMeasuredTypes(trueTypes, measuredTypes, allowEmpty = True)):
            # self.log.debug(f"analyzeTrueAndMeasuredTypes('{trueTypes}', '{measuredTypes}') returning INVALID")
            return TrueMeasuredCategory.INVALID
        if trueTypes != "" and measuredTypes != "":
            # both true and measured types are defined
            # self.log.debug(f"analyzeTrueAndMeasuredTypes('{trueTypes}', '{measuredTypes}') returning TRUEANDMEASURED.")
            return TrueMeasuredCategory.TRUEANDMEASURED
        elif trueTypes != "" and measuredTypes == "":
            # only true types are defined
            # self.log.debug(f"analyzeTrueAndMeasuredTypes('{trueTypes}', '{measuredTypes}') returning TRUE.")
            return TrueMeasuredCategory.TRUE
        elif trueTypes == "" and measuredTypes != "":
            # only measured types are defined
            # self.log.debug(f"analyzeTrueAndMeasuredTypes('{trueTypes}', '{measuredTypes}') returning MEASURED.")
            return TrueMeasuredCategory.MEASURED
        elif trueTypes == "" and measuredTypes == "":
            # overall yield is requested
            # self.log.debug(f"analyzeTrueAndMeasuredTypes('{trueTypes}', '{measuredTypes}') returning OVERALL.")
            return TrueMeasuredCategory.OVERALL
        else:
            self.log.error("Internal inconsistency discovered. This line should never be executed.")
        return TrueMeasuredCategory.INVALID


    # returns list of all combination of true types
    def getTrueTypesList(self):
        return [self.indexToTrueTypes(index) for index in range(self.dimension)]

    # returns list of all combination of measured types
    def getMeasuredTypesList(self):
        return [self.indexToMeasuredTypes(index) for index in range(self.dimension)]


    ### Useful general methods ###

    @staticmethod
    def valuesEqual(result1, result2, ignoreNone = True):
        # Returns true if two values are approximately equal.
        if result1 == None or result2 == None:
            return True
        if result1 == result2:
            return True
        if result1 == 0:
            # avoid division by 0
            result1 = result2
            result2 = 0
        if abs((result1 - result2)/result1) < 1e-9:
            return True
        else:
            return False

    @staticmethod
    def compareResultsAndReturnSensibleOne(result1, result2, printErrors = True, exitOnError = True):
        if result1 == None:
            return result2
        elif result2 == None:
            return result1
        elif exitOnError and not EquationBase.valuesEqual(result1, result2):
            print("ERROR in (compareResultsAndReturnSensibleOne(...)): consistency check of stored values failed. Exiting.")
            print(f"result1 = {result1}, result2 = {result2}")
            self.printStoredValues()
            sys.exit(1)
        else:
            return result1

    @staticmethod
    def efficiencyToFakeFactor(efficiency):
        if efficiency == 0:
            return 0
        return (efficiency / (1 - efficiency))

    @staticmethod
    def fakeFactorToEfficiency(fakeFactor):
        if fakeFactor == -1:
            log.ERROR("Trying to convert a fake factor of {fakeFactor} to efficiency. This will be a division by 0.")
        return (fakeFactor / (1 + fakeFactor))

    ### Methods to be overwritten by inheriting class ###

    def getNEvents(self, trueTypes, measuredTypes):
        raise NotImplementedError("Classes inheriting from EquationBase must implement getNEvents(trueTypes, measuredTypes)")
