#!/usr/bin/env python3

import pickle
import xarray as xr
import numpy as np
import scipy
import argparse
from faketoys import utils, log
import dask.diagnostics
import os

def main(args):
    configName = args.config.split("/")[-1]
    configName = ".".join(configName.split(".")[0:-1])
    sbatchFile = utils.getOutputPath("submit/" + configName + "_sbatch.sh")
    runFile = utils.getOutputPath("submit/" + configName + "_run.sh")
    logFilePrep = utils.getOutputPath("submit/" + configName + "_log_prepare.txt")
    logFileComp = utils.getOutputPath("submit/" + configName + "_log_compute.txt")

    dt = args.progress_dt
    if args.progress_dt == -1:
        # If not specified in CLI, update progress 100 times during
        # compute, but min/max interval is 10 seconds/15 minutes.
        dt = args.time * 60 / 100.
        dt = max(dt, 10)
        dt = min(dt, 60*15)

    s = ""
    if args.random_seed != None:
        s = f"--random_seed {args.random_seed}"

    if not "FAKEBASE" in os.environ:
        log.ERROR("Please define the environment variable FAKEBASE as the base directory of your git repository.")
        return False
    fakebase = os.environ["FAKEBASE"]
    if fakebase[-1] == "/":
        fakebase = fakebase[0:-1]
    with open(sbatchFile, "w") as f:
        f.write( "#!/bin/bash\n")
        f.write( "#SBATCH --account=ctb-stelzer\n")
        f.write( "#SBATCH --mem-per-cpu=4000\n")
        f.write(f"#SBATCH --cpus-per-task={args.num_workers}\n")
        f.write(f"#SBATCH --time={args.time}\n\n")
        f.write(f"cd {fakebase}\n")
        f.write( "module load singularity\n")
        f.write(f"chmod u+x {runFile}\n")
        f.write(f"singularity exec -e docker://kolehman/faketoys:base {runFile}\n")

    log.INFO(f"Wrote {sbatchFile}")

    with open(runFile, "w") as f:
        f.write( "#!/bin/bash\n\n")
        f.write(f"echo 'starting {runFile}'\n")
        f.write(f"cd {fakebase}\n")
        f.write( "source setup.sh\n")
        f.write(f"cd {fakebase}/faketoys\n")
        f.write(f"echo 'check log file at {logFilePrep}'\n")
        f.write(f"./prepare.py {args.config} > {logFilePrep} 2>&1\n")
        f.write(f"echo 'check log file at {logFileComp}'\n")
        f.write(f"./compute.py {args.config} --progress_dt {dt} --parallelize --num_workers {args.num_workers} {s} > {logFileComp} 2>&1\n")
        f.write(f"echo 'finishing {runFile}'\n")

    log.INFO(f"Wrote {runFile}")

    log.INFO(f"Once submitted, the log files will be written to {logFilePrep} and {logFileComp}")
    log.INFO(f"Submit with\nsbatch {sbatchFile}")
    return True

if __name__ == '__main__':
    parser = utils.getArgParserSubmit()
    args = parser.parse_args()
    if args.num_workers == -1:
        log.ERROR("You must set --num_workers.")
    else:
        main(args)
