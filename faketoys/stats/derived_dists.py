from faketoys.stats import derived_dist_base, dist_base
from faketoys import utils
import numpy as np

class FakeFactor(derived_dist_base.DerivedDistributionBase):
    '''Calculate the Fake Factor from four underlying distributions.

    The fake factor is calculated by measuring the number of events with
    tight and loose leptons and taking their ratio after subtracting the
    number of events with real contribution: F = (Nt - Ntr) / (Nl - Nlr)
    '''

    def __init__(self, Nt, Ntr, Nl, Nlr, name = None, denominatorGreaterThan=-float("inf")):
        '''Initialize FakeFactor.

        The arguments Nt, Ntr, Nl, Nlr have to inherit from
        :py:class:`~faketoys.stats.dist_base.DistributionBase`.

        Args:
            Nt (DistributionBase): Distribution of events with tight
                lepton.
            Ntr (DistributionBase): Distribution of events with real,
                tight lepton.
            Nl (DistributionBase): Distribution of events with loose
                lepton.
            Nlr (DistributionBase): Distribution of events with real,
                loose lepton.
            name (str): Name of the instance.
            denominatorGreaterThan (float): Minimum acceptable value of
                denominator. See :py:func:`calculateValue`.
        '''
        super().__init__(name = name)

        if not isinstance(Nt, dist_base.DistributionBase):
            FakeFactor.ERROR("Argument Nt needs to be an instance of DistributionBase")
        if not isinstance(Ntr, dist_base.DistributionBase):
            FakeFactor.ERROR("Argument Ntr needs to be an instance of DistributionBase")
        if not isinstance(Nl, dist_base.DistributionBase):
            FakeFactor.ERROR("Argument Nl needs to be an instance of DistributionBase")
        if not isinstance(Nlr, dist_base.DistributionBase):
            FakeFactor.ERROR("Argument Nlr needs to be an instance of DistributionBase")
        self.Nt  = Nt
        self.Ntr = Ntr
        self.Nl  = Nl
        self.Nlr = Nlr
        self.denominatorGreaterThan = denominatorGreaterThan
        self.setUnderlyingDistributions({"Nt" : Nt,
                                         "Ntr": Ntr,
                                         "Nl" : Nl,
                                         "Nlr": Nlr})
        return

    @staticmethod
    def calculateFF(Nt, Ntr, Nl, Nlr):
        '''Calculate the fake factor for four given values.

        Args:
            Nt (float): Number of events with tight lepton.
            Ntr (float): Number of events with real, tight lepton.
            Nl (float): Number of events with loose lepton.
            Nlr (float): Number of events with real, loose lepton.
        Returns:
            float: Fake factor.
        '''
        if utils.variablesEqual(Nl, Nlr):
            return float("inf")
        return (Nt - Ntr) / (Nl - Nlr)

    def calculateValue(self, dictOfValues, denominatorProtection=True):
        '''Caluclates the fake factor from a dict.

        The keys of the dict have to be the strings "Nt", "Ntr", "Nl",
        "Nlr". The fake factor is calculated from the items of the dict.

        Args:
            dictOfValues (dict): dict with values derived from the
              underlying distributions. The keys of the dict have to be
              the strings "Nt", "Ntr", "Nl", "Nlr". The fake factor is
              calculated from the items of the dict.
            denominatorProtection (bool): If True and the denominator is
              smaller than `denomintorGreaterThan` (specified in
              constructor), then the denominator is set to the
              value `denominatorGreaterThan`.

        Returns:
            float: Fake factor.
        '''

        Nt = dictOfValues["Nt"]
        Ntr = dictOfValues["Ntr"]
        Nl = dictOfValues["Nl"]
        Nlr = dictOfValues["Nlr"]
        if denominatorProtection and (Nl - Nlr) < self.denominatorGreaterThan:
            Nl = self.denominatorGreaterThan
            Nlr = 0
        return FakeFactor.calculateFF(Nt, Ntr, Nl, Nlr)

    def __repr__(self):
        if self.name == None:
            return f"FakeFactor({self.getTrueValue()})"
        else:
            return f"FakeFactor({self.getTrueValue()}, {self.name})"




class SumOfDistributions(derived_dist_base.DerivedDistributionBase):
    '''Calculates the sum of distributions.'''

    def __init__(self, listOfDistributions, name = None):
        '''Initialize the sum.

        Args:
            listOfDistributions (list): List of instances inheriting
                from DistributionBase.
            name (str): Name of the instance.
        '''
        super().__init__(name = name)
        if not isinstance(listOfDistributions, list):
            SumOfDistributions.ERROR(f"Constructor expects a list of distributions. Got a {type(listOfDistributions)} instead.")
        d = {}
        for i, distribution in enumerate(listOfDistributions):
            if not isinstance(distribution, dist_base.DistributionBase):
                SumOfDistributions.ERROR(f"Constructor expects a list of distributions. Entry {i} is a {type(distribution)}.")
            d[i] = distribution

        self.setUnderlyingDistributions(d)
        return

    @staticmethod
    def calculateValue(dictOfValues):
        '''Adds all values of a dict. This method defines the
        mathematical dependence on the underlying distributions.

        Args:
            dictOfValues (dict): dict with values derived from the
              underlying distributions. The keys of the dict are
              irrelevant.

        Returns:
            float: Sum of values in `dictOfValues`.
        '''
        return sum(dictOfValues.values())

    def getStddev(self):
        if not self.hasUnderlyingDistributions():
            SumOfDistributions.WARNING("(getStddev) No underlying distributions set. Returning 0.")
            return 0
        SumOfDistributions.DEBUG("Assuming that distributions can be combined in quadrature.")
        listOfSum2 = [(x.getStddev()**2) for x in self.underlyingDistributions.values()]
        return np.sqrt(sum(listOfSum2))

    def __repr__(self):
        if self.name == None:
            return f"Sum({self.getTrueValue()})"
        else:
            return f"Sum({self.getTrueValue()}, {self.name})"




class ProductOfDistributions(derived_dist_base.DerivedDistributionBase):
    '''Calculates the product of distributions'''

    def __init__(self, listOfDistributions, name = None):
        '''Initialize the product.

        Args:
            listOfDistributions (list): List of instances inheriting
                from DistributionBase.
            name (str): Name of the instance.
        '''
        super().__init__(name = name)
        if not isinstance(listOfDistributions, list):
            ProductOfDistributions.ERROR(f"Constructor expects a list of distributions. Got a {type(listOfDistributions)} instead.")
        d = {}
        for i, distribution in enumerate(listOfDistributions):
            if not isinstance(distribution, dist_base.DistributionBase):
                ProductOfDistributions.ERROR(f"Constructor expects a list of distributions. Entry {i} is a {type(distribution)}.")
            d[i] = distribution

        self.setUnderlyingDistributions(d)
        return

    @staticmethod
    def calculateValue(dictOfValues):
        '''Multiplies all values of a dict. This method defines the
        mathematical dependence on the underlying distributions.

        Args:
            dictOfValues (dict): dict with values derived from the
              underlying distributions. The keys of the dict are
              irrelevant.

        Returns:
            float: Product of values in `dictOfValues`.
        '''
        # When updating to python3.8, can use math.prod()
        product = 1
        for value in dictOfValues.values():
            product *= value
        return product

    def __repr__(self):
        if self.name == None:
            return f"Product({self.getTrueValue()})"
        else:
            return f"Product({self.getTrueValue()}, {self.name})"
