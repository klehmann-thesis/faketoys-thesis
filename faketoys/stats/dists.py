from faketoys.stats import dist_base
import numpy as np
import scipy.special
import scipy.stats
from uncertainties import ufloat

class Gaussian(dist_base.DistributionBase):
    '''
    Gaussian distribution with custom mean and standard deviation.
    '''

    def __init__(self, mean, stddev, relStddev=False, name=None):
        '''Initialize Gaussian distribution.

        Args:
            mean (int, float): Mean of the Gaussian.
            stddev (int, float): Standard deviation of the Gaussian.
            relStddev (bool): If False, then stddev is always used at
              face value. If True, then stddev is treated relative to
              the mean value. Thus, if a random variable r is drawn, the
              standard deviation will be (r * stddev / mean). Do not use
              this option with ``mean=0``, because this would correspond
              to an infinite relative uncertainty.
            name (str): Name of the instance.
        '''
        super().__init__(name=name)
        Gaussian.DEBUG(f"Initializing with mean {mean}, stddev {stddev}, name {name}.")
        self.stddev = float(stddev)
        self.mean = float(mean)
        if not isinstance(relStddev, bool):
            Gaussian.WARNING(f"Interpreting the variable relStddev ('{relStddev}') as boolean.")
        self.relStddev = relStddev
        if self.relStddev and mean == 0:
            Gaussian.ERROR(f"Initializing Gaussian with mean {mean}, stddev {stddev}, relStddev {relStddev}, name {name}. Trying to interpret standard deviation as relative standard deviation. This doesn't make any sense. It can also cause a crash later becuase of a division by 0.")
            sys.exit()
        return

    def getProbabilityDensity(self, x):
        '''Evaluate probability density at the value x.

        Args:
            x (int, float): x-value, on which PDF is evaluated.

        Returns:
            float: probability density
        '''
        normFactor = 1/(np.sqrt(2*np.pi)*self.stddev)
        return normFactor * np.exp(-0.5 * ((x-self.mean)/self.stddev)**2)

    def getTrueValue(self):
        '''Same as :py:func:`getMean`'''
        return self.getMean()

    def getMean(self):
        return self.mean

    def getStddev(self, cacheIndex=None):
        '''Get the standard deviation.

        This method has two possible return values. By default, it
        returns the true standard deviation of the Gaussian that was
        given as argument in the constructor. Alternatively, the
        standard deviation is estimated based on the recent
        "measurement" ``r``, i.e. random variable, using the equation
        ``r * stddev / mean``.

        Args:
            cacheIndex (int): If None, the true standard deviation is
              returned. If an integer, the ``cacheIndex`` is resolved.
              If this is not possible (no matching ``cacheIndex`` is
              found) or ``relStddev=False``, the true standard deviation
              is returned. Otherwise, the reestimated standard deviation
              is returned.
        Returns:
            float: Standard deviation
        '''
        if cacheIndex == None or cacheIndex != self.cacheIndex or not self.relStddev:
            return self.stddev
        cachedValue = self.getCachedValue(cacheIndex)
        return cachedValue * self.stddev / self.mean

    def getVariationsForStatUnc(self, cacheIndex=None):
        '''Get the up and down variations.

        Args:
            cacheIndex (int): Is interpreted like in
              :py:meth:`getStddev`.
        Returns:
            list: standard deviation [up, down]
        '''
        stddev = self.getStddev(cacheIndex=cacheIndex)
        return [+stddev, -stddev]

    def _getRandom(self, rng, nRandom=None):
        '''Draw random variable.

        Args:
            nRandom (int): Number of random variables.

        Returns:
            float, list: Random variable (if nRandom is None) or a list
              of random variables.
        '''
        retVal = rng.normal(self.mean, self.stddev, nRandom)
        if isinstance(retVal, np.ndarray):
            return retVal.tolist()
        return retVal

    def __repr__(self):
        s = f"Gaussian({self.mean}, {self.stddev}"
        if self.relStddev:
            s = s + ", {self.relStddev}"
        if self.name == None:
            return s + ")"
        else:
            return s + f", {self.name})"




class Poisson(dist_base.DistributionBase):
    '''
    Poisson distribution with custom mean.
    '''

    def __init__(self, mean, name=None):
        '''Initialize Poisson distribution.

        Args:
            mean (int, float): Mean and variance of the distribution.
            name (str): Name of the instance.
        '''
        super().__init__(name=name)
        Poisson.DEBUG(f"Initializing with mean {mean}.")
        self.variance = float(mean)
        self.mean = float(mean)
        return

    def getProbabilityDensity(self, x):
        '''Evaluate probability density at the value x.

        Args:
            x (int, float): x-value, on which PDF is evaluated.

        Returns:
            float: probability density
        '''
        return scipy.stats.poisson.pmf(x, self.mean)

    def getTrueValue(self):
        '''Same as :py:func:`getMean`'''
        return self.getMean()

    def getMean(self):
        return self.mean

    def getStddev(self, cacheIndex=None):
        '''Get the standard deviation.

        This method has two possible return values. By default, it
        returns the square root of the variance of the distribution
        that was given as argument in the constructor. Alternatively,
        the standard deviation is estimated based on the recent
        "measurement" ``r``, i.e. random variable, using the equation
        ``sqrt(r)``.

        Args:
            cacheIndex (int): If None, the true standard deviation is
              returned. If an integer, the ``cacheIndex`` is resolved.
              If this is not possible (no matching ``cacheIndex`` is
              found), the true standard deviation is returned.
              Otherwise, the reestimated standard deviation is returned.
        Returns:
            float: Standard deviation
        '''
        if cacheIndex == None or cacheIndex != self.cacheIndex:
            return np.sqrt(self.variance)
        cachedValue = self.getCachedValue(cacheIndex)
        return np.sqrt(cachedValue)

    def getVariationsForStatUnc(self, cacheIndex=None):
        '''
        Get the up and down variations.

        Args:
            cacheIndex (int): If no `cacheIndex` is provided the
              variations are estimated based on the true value. If a
              cacheIndex is provided (and the corresponding value is
              actually cached), it is used to estimate the variation.

        Returns:
            list: standard deviation [up, down]
        '''
        stddev = self.getStddev(cacheIndex=cacheIndex)
        return [+stddev, -stddev]

    def _getRandom(self, rng, nRandom=None):
        '''Draw random variable.

        Args:
            nRandom (int): Number of random variables.

        Returns:
            float, list: Random variable (if nRandom is None) or a list
              of random variables.
        '''
        retVal = rng.poisson(self.mean, nRandom)
        if isinstance(retVal, np.ndarray):
            return retVal.tolist()
        return retVal

    def __repr__(self):
        if self.name == None:
            return f"Poisson({self.mean})"
        else:
            return f"Poisson({self.mean}, {self.name})"




class Delta(dist_base.DistributionBase):
    '''
    Delta distribution. It always returns the same, true value.
    '''

    def __init__(self, value, name=None):
        '''Initialize Delta distribution.

        Args:
            value (int, float): True value of Delta distribution.
            name (str): Name of the instance.
        '''
        super().__init__(name=name)
        Delta.DEBUG(f"Initializing with value {value}.")
        self.value = float(value)
        return

    def getTrueValue(self):
        return self.value
    def getMean(self):
        '''Same as :py:func:`getTrueValue`.'''
        return self.getTrueValue()
    def getStddev(self):
        '''Returns 0.'''
        return 0
    def getVariationsForStatUnc(self, cacheIndex=None):
        '''Returns empty list.'''
        return []
    def _getRandom(self, rng=None, nRandom=None):
        if nRandom == None:
            return self.getTrueValue()
        return [self.getTrueValue() for x in range(nRandom)]

    def __repr__(self):
        if self.name == None:
            return f"Delta({self.value})"
        else:
            return f"Delta({self.value}, {self.name})"




class Dummy(dist_base.DistributionBase):
    """Dummy probability distribution.

    Sometimes, it doesn't make sense to define a real probability
    distribution, but a dummy class can be helpful to avoid special
    treatment of these cases. Unlike other distributions, this
    distribution has no special properties and returns empty lists
    and NAN.
    """

    def __init__(self, name=None):
        '''Initialize with name.

        Args:
            name (str): Name of the instance.
        '''
        super().__init__(name=name)
        return

    def getTrueValue(self):
        return np.nan
    def getMean(self):
        return np.nan
    def getStddev(self):
        return np.nan
    def getVariationsForStatUnc(self, cacheIndex=None):
        return []
    def _getRandom(self, rng=None, nRandom=None):
        if not nRandom == None:
            return [np.nan for x in range(nRandom)]
        return np.nan
    def getExpression(self, statOnly=False):
        return ufloat(0, 0, "Dummy")
    def __repr__(self):
        if self.name == None:
            return f"Dummy"
        else:
            return f"Dummy({self.name})"



class Uniform(dist_base.DistributionBase):
    '''
    Uniform distribution.
    '''

    def __init__(self, minimum, maximum, trueValue, name=None):
        '''Initialize uniform distribution.

        Args:
            minimum (int, float): Minimum of uniform distribution.
            maximum (int, float): Maximum of uniform distribution.
            trueValue (int, float): True value.
            name (str): Name of the instance.
        '''
        super().__init__(name=name)
        Uniform.DEBUG(f"Initializing with minimum {minimum}, maximum {maximum}, true value {trueValue}.")
        self.minimum = float(minimum)
        self.maximum = float(maximum)
        if self.minimum > self.maximum:
            Uniform.WARNING(f"The maximum {self.maximum} of the uniform distribution is smaller than the minimum {self.minimum}.")
        self.trueValue = float(trueValue)
        return

    def getProbabilityDensity(self, x):
        '''Evaluate probability density at x.

        Args:
            x (int, float): x-value, on which PDF is evaluated.

        Returns:
            float: probability density
        '''
        if self.minimum < x and x < self.maximum:
            return 1./self.getRange()
        else:
            return 0.

    def getTrueValue(self):
        '''Get true value.'''
        return self.trueValue

    def getMean(self):
        return (self.maximum+self.minimum) / 2.

    def getRange(self):
        return self.maximum-self.minimum

    def getStddev(self, cacheIndex=None):
        '''Get the standard deviation.

        Standard deviation is calculated with
        ``maximum-minimum / sqrt(12)``.

        Args:
            cacheIndex (int): Argument is ignored.
        Returns:
            float: Standard deviation
        '''
        return self.getRange() / np.sqrt(12)

    def getVariationsForStatUnc(self, cacheIndex=None):
        '''
        Get the up and down variations.

        Args:
            cacheIndex (int): Argument is irrelevant because it is
              ignored by :py:func:`getStddev`.

        Returns:
            list: standard deviation [up, down]
        '''
        stddev = self.getStddev(cacheIndex=cacheIndex)
        return [+stddev, -stddev]

    def _getRandom(self, rng, nRandom=None):
        '''Draw random variable.

        Args:
            nRandom (int): Number of random variables.

        Returns:
            float, list: Random variable (if nRandom is None) or a list
              of random variables.
        '''
        retVal = rng.uniform(self.minimum, self.maximum, nRandom)
        if isinstance(retVal, np.ndarray):
            return retVal.tolist()
        return retVal

    def __repr__(self):
        if self.name == None:
            return f"Uniform({self.minimum}, {self.maximum})"
        else:
            return f"Uniform({self.minimum}, {self.maximum}, {self.name})"
