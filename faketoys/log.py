from datetime import datetime, timedelta

class Level:
    DEBUG    = 0
    VERBOSE  = 10
    INFO     = 20
    WARNING  = 30
    ERROR    = 40
    CRITICAL = 50

def DEBUG(message):
    '''See :py:meth:`Logger.DEBUG`.'''
    Logger.DEBUG(message)

def VERBOSE(message):
    '''See :py:meth:`Logger.VERBOSE`.'''
    Logger.VERBOSE(message)

def INFO(message):
    '''See :py:meth:`Logger.INFO`.'''
    Logger.INFO(message)

def WARNING(message):
    '''See :py:meth:`Logger.WARNING`.'''
    Logger.WARNING(message)

def ERROR(message):
    '''See :py:meth:`Logger.ERROR`.'''
    Logger.ERROR(message)

def CRITICAL(message):
    '''See :py:meth:`Logger.CRITICAL`.'''
    Logger.CRITICAL(message)

def setLevel(level):
    '''Set log level for :py:class:`Logger`.'''
    Logger.logLevel = level

class Logger:
    """Class to allow basic logging.

    Inherit from :py:class:`Logger` if you want to control logging
    levels for each inheriting class (as opposed to each instance). To
    specify a desired log level for a class, set the member attribute::

      logLevel = log.Level.VERBOSE

    This will cause all commands of the severity VERBOSE or higher to be
    printed. For a full list of log levels, see below.

    In your code use the command::

      InheritingClass.VERBOSE("<verbose message>")

    to define a log output. This message will be compared against the
    log level of InheritingClass. To emit a general logging message that
    is not associated to a derived class and uses the logging level of
    the base :py:class:`Logger`, you can use one of the two following,
    equivalent lines::

      log.VERBOSE("<verbose message>")
      log.Logger.VERBOSE("<verbose message>")

    All available log levels (ordered by incresing severity) are

    * DEBUG
    * VERBOSE
    * INFO
    * WARNING
    * ERROR
    * CRITICAL
    """

    logLevel = Level.INFO
    _startTime = datetime.now()

    @classmethod
    def _printMessage(cls, level, message):
        end = datetime.now()
        delta = (str(end - Logger._startTime)).split(".")[0]

        if cls.__name__ == "Logger":
            print("%s %s: %s" % (delta, level, message))
        else:
            print("%s %s (%s): %s" % (delta, level, cls.__name__, message))

    @classmethod
    def DEBUG(cls, message):
        '''Emit a message if :py:attr:`logLevel` is appropriate.'''
        if cls.logLevel <= Level.DEBUG:
            cls._printMessage("DEBUG", message)

    @classmethod
    def VERBOSE(cls, message):
        '''Emit a message if :py:attr:`logLevel` is appropriate.'''
        if cls.logLevel <= Level.VERBOSE:
            cls._printMessage("VERBOSE", message)

    @classmethod
    def INFO(cls, message):
        '''Emit a message if :py:attr:`logLevel` is appropriate.'''
        if cls.logLevel <= Level.INFO:
            cls._printMessage("INFO", message)

    @classmethod
    def WARNING(cls, message):
        '''Emit a message if :py:attr:`logLevel` is appropriate.'''
        if cls.logLevel <= Level.WARNING:
            cls._printMessage("WARNING", message)

    @classmethod
    def ERROR(cls, message):
        '''Emit a message if :py:attr:`logLevel` is appropriate.'''
        if cls.logLevel <= Level.ERROR:
            cls._printMessage("ERROR", message)

    @classmethod
    def CRITICAL(cls, message):
        '''Emit a message if :py:attr:`logLevel` is appropriate.'''
        if cls.logLevel <= Level.CRITICAL:
            cls._printMessage("CRITICAL", message)

    def __init__(self, name=""):
        self.name = name
