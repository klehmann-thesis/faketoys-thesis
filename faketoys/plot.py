from faketoys import log, utils
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import xarray as xr
import matplotlib.ticker

def getDefaultFigsize():
    return (5.6, 4.2)

def combineListOfVariedValues(listOfVariedValues, nominalValue):
    varUp2   = 0
    varDown2 = 0
    for variedValue in listOfVariedValues:
        variation = variedValue - nominalValue
        if variation > 0:
            varUp2 += variation**2
        else:
            varDown2 += variation**2
    varUp   = np.sqrt(varUp2)
    varDown = np.sqrt(varDown2)
    return (varUp, varDown)

def pValue_fromDataset(dataset, plotRangeX=None, histogramName="h_pValue"):
    bins = dataset[histogramName].item()[1]
    weights = dataset[histogramName].item()[0]
    _ = plt.hist(bins[0:-1], weights=weights, bins=bins)
    return

def FFE_fromDataset(dataset, saveAs=None, plotRangeX=None, plotRangeY=None, histogramName="h_random", **kwargs):
    # kwargs forwarded to FFE
    # Wrapper method around FFE
    if histogramName in dataset:
        histogram = dataset[histogramName].values.item()
    elif "histogram" in dataset:
        # For backwards compatibility
        log.WARNING(f"Didn't find histogram with name '{histogramName}', but one with name 'histogram'. Using this one instead.")
        histogram = dataset["histogram"].values.item()
    else:
        log.ERROR(f"plot.FFE_fromDataset() Cannot find histogram with name '{histogramName}' or 'histogram' in dataset.")
        return
    dsArguments = ["median",
                   "trueValue",
                   "mean",
                   "mean_unc",
                   "stddev",
                   "asymStddev",
                   # "asymStddevGreater0",
                   "expression",
                   "listOfVariedValues"]

    for dsArgument in dsArguments:
        # do not overwrite kwargs
        if dsArgument in dataset and dsArgument not in kwargs:
            kwargs[dsArgument] = dataset[dsArgument]

    FFE(histogram[0],
        histogram[1],
        saveAs=saveAs,
        plotRangeX=plotRangeX,
        plotRangeY=plotRangeY,
        **kwargs,
        )
    return

def crop2dArray(weights, bins, plotRangeX=None, plotRangeY=None):
    """Crops 2d numpy array by comparing plotRange to bin values.

    bins is a tuple of bins for the x and y axis. plotRangeX/Y are a
    tuple indicating a range of x and y-axis values. If they are set to
    None, nothing is cropped. This method returns a tuple of new
    (weights, bins).
    """
    newBins = [bins[0], bins[1]]
    newWeights = weights
    if not plotRangeX == None:
        for xBinMin, xMin in enumerate(bins[0]):
            if xMin >= plotRangeX[0]:
                break
        for xBinMax, xMax in enumerate(bins[0]):
            if xMax >= plotRangeX[1]:
                break
        newBins[0] = newBins[0][xBinMin:xBinMax+1]
        newWeights = newWeights[xBinMin:xBinMax, :]
    if not plotRangeY == None:
        for yBinMin, yMin in enumerate(bins[1]):
            if yMin >= plotRangeY[0]:
                break
        for yBinMax, yMax in enumerate(bins[1]):
            if yMax >= plotRangeY[1]:
                break
        newBins[1] = newBins[1][yBinMin:yBinMax+1]
        newWeights = newWeights[:, yBinMin:yBinMax]
    return newWeights, newBins

def randomVsPValue_fromDataset(ds, withProjections=True, histogramName="h_random_pValue", xlabel="Fake Factor estimate", ylabel="$p(x_t)$", colorbarLabel="Count", **kwargs):
    weights = ds[histogramName].item()[0]
    bins = ds[histogramName].item()[1]
    args = [weights, bins]

    kwargs["xlabel"] = xlabel
    kwargs["ylabel"] = ylabel
    kwargs["colorbarLabel"] = colorbarLabel

    if withProjections:
        return histo2dWithProjection(*args, **kwargs)
    else:
        return histo2d(*args, **kwargs)

def histo2dWithProjection(weights, bins, plotRangeX=None, plotRangeY=None, saveAs=None, xlabel=None, ylabel=None, colorbarLabel=None):
    '''Plot 2d histogram with 1d projections.

    Uses the function py:func:`histo2d` for the scatter plot and adds
    the projections.

    Args:
        weights: 2d array with weights of 2d histogram.
        bins: Two 1d arrays with bin boundaries.
        plotRangeX (tuple): Set manual plot range like ``(0, 2.1)``.
        plotRangeY (tuple): Set manual plot range like ``(0, 2.1)``.
        saveAs (bool): Filename to save figure as.
        xlabel (str): Axis label on x axis.
        ylabel (str): Axis label on y axis.
        colorbarLabel (str): Axis label on color axis.

    Returns:
        matplotlib.figure.Figure, dict: Figure. All used
        ``matplotlib.axes.Axes`` with the keys ``scatter``, ``histX``,
        ``histY``, ``colorbar``.

    '''
    weights, bins = crop2dArray(weights, bins, plotRangeX, plotRangeY)

    nullfmt = matplotlib.ticker.NullFormatter()
    left, width = 0.13, 0.62
    bottom, height = 0.13, 0.62
    bottom_h = left_h = left + width + 0.02

    rect_scatter = [left, bottom, width, height]
    rect_histx = [left, bottom_h, width, 0.2]
    rect_histy = [left_h, bottom, 0.2, height]

    fig = plt.figure(figsize=(6, 6))

    axScatter = plt.axes(rect_scatter)
    axHistx = plt.axes(rect_histx)
    axHisty = plt.axes(rect_histy)
    if colorbarLabel:
        colorbarLeft = left+width-0.1
    else:
        colorbarLeft = left+width-0.08
    axColorbar = plt.axes([colorbarLeft, bottom+0.05, 0.05, height-0.1])

    # no labels
    axHistx.xaxis.set_major_formatter(nullfmt)
    axHisty.yaxis.set_major_formatter(nullfmt)
    axHistx.set_ylabel("Count")
    axHisty.set_xlabel("Count")

    # Pass plotRangeX/Y=None, because the weights are already cropped.
    histo2d(weights, bins, plotRangeX=None, plotRangeY=None, axes=axScatter, colorbarAxes=axColorbar, fig=fig, xlabel=xlabel, ylabel=ylabel, colorbarLabel=colorbarLabel)

    projectionX = []
    projectionY = []
    for x in range(weights.shape[0]):
        projectionX.append(sum(weights[x, :]))
    for y in range(weights.shape[1]):
        projectionY.append(sum(weights[:, y]))

    axHistx.hist(bins[0][:-1], weights=np.asarray(projectionX), bins=bins[0])
    axHisty.hist(bins[1][:-1], weights=np.asarray(projectionY), bins=bins[1], orientation='horizontal')

    if plotRangeX == None:
        axHistx.set_xlim(bins[0][0], bins[0][-1])
    else:
        axHistx.set_xlim(*plotRangeX)
    if plotRangeY == None:
        axHisty.set_ylim(bins[1][0], bins[1][-1])
    else:
        axHisty.set_ylim(*plotRangeY)


    if not saveAs == None:
        outPath = utils.getOutputPath(saveAs)
        log.INFO(f"Plotting {outPath}")
        plt.savefig(outPath)

    axes = {
        'scatter': axScatter,
        'histX': axHistx,
        'histY': axHisty,
        'colorbar': axColorbar}

    return fig, axes

def histo1d(weights, bins, plotRangeX=None, plotRangeY=None, axes=None, xlabel=None, ylabel=None):
    ax = axes
    if not axes:
        fig, ax = plt.subplots()

    if isinstance(plotRangeX, tuple):
        ax.set_xlim(*plotRangeX)
    if isinstance(plotRangeY, tuple):
        ax.set_ylim(*plotRangeY)

    # plot histogram
    ax.hist(bins[:-1], bins=bins, weights=weights)
    ax.grid()
    if xlabel:
        ax.set_xlabel(xlabel)
    if ylabel:
        ax.set_ylabel(ylabel)

def histo2d(weights, bins, plotRangeX=None, plotRangeY=None, axes=None, colorbarAxes=None, fig=None, xlabel=None, ylabel=None, colorbarLabel=None, saveAs=None):
    newWeights, newBins = crop2dArray(weights, bins, plotRangeX, plotRangeY)
    kwargs = {"X": np.swapaxes(newWeights, 0, 1),
              "origin": "lower",
              "cmap": "Blues",
              "extent": [newBins[0][0], newBins[0][-1], newBins[1][0], newBins[1][-1]],
              "aspect": "auto"
             }
    if axes:
        im = axes.imshow(**kwargs)
        if colorbarAxes and fig:
            fig.colorbar(im, cax=colorbarAxes, ax=axes, orientation="vertical")
            colorbarAxes.get_yaxis().set_ticks_position("both")
            colorbarAxes.get_yaxis().tick_left()
            if colorbarLabel:
                colorbarAxes.set_ylabel(colorbarLabel)
    else:
        axes = plt.imshow(**kwargs)
        colorbar = plt.colorbar()
        if colorbarLabel:
            colorbar.set_label(colorbarLabel)

    if xlabel:
        axes.set_xlabel(xlabel)
    if ylabel:
        axes.set_ylabel(ylabel)

    if not saveAs == None:
        outPath = utils.getOutputPath(saveAs)
        log.INFO(f"Plotting {outPath}")
        plt.savefig(outPath)
    return fig


def invertIndexIterator(indexIt, array, invertX=False, invertY=False):
    '''Inverts element order of multi_index np.nditer (1d and 2d)

    Pass a multi_index from np.nditer as indexId and the array from
    which it was created as array. If invertX or invertY are true,
    the indices of the corresponding axes are inverted.
    '''
    if not invertX and not invertY:
        return indexIt
    retVal = list(indexIt)
    if len(indexIt) == 2:
        if invertY:
            lenY = array.shape[0]
            retVal[0] = lenY-retVal[0]-1
        if invertX:
            lenX = array.shape[1]
            retVal[1] = lenX-retVal[0]-1
    elif len(indexIt) == 1:
        if invertX:
            lenX = array.shape[0]
            retVal[0] = lenX-retVal[0]-1
    return retVal

def label_outer_onlyX(axis):
    '''Hide x labels and ticks if axis is in bottom row of subplot.

    Pass the axis of a matplotlib subplot to remove the x-axis labels
    and ticks of this plot if it is not in the bottom row. This
    function is a re-implementation of the method
    matplotlib.axes.SubplotBase.label_outer() for only the x axis.

    The complementary method is label_outer_onlyY().
    '''
    if not isinstance(axis, matplotlib.axes.SubplotBase):
        log.WARNING("The function plot.label_outer_onlyY(axis) only works for an axis of a matplotlib subplot. Returning without changing the labels.")
        return
    lastrow = axis.is_last_row()
    if not lastrow:
        for label in axis.get_xticklabels(which="both"):
            label.set_visible(False)
        axis.get_xaxis().get_offset_text().set_visible(False)
        axis.set_xlabel("")

def label_outer_onlyY(axis):
    '''Hide y labels and ticks if axis is in left column of subplot.

    See documentation of complementary function label_outer_onlyX().
    '''
    if not isinstance(axis, matplotlib.axes.SubplotBase):
        log.WARNING("The function plot.label_outer_onlyY(axis) only works for an axis of a matplotlib subplot. Returning without changing the labels.")
        return
    firstcol = axis.is_first_col()
    if not firstcol:
        for label in axis.get_yticklabels(which="both"):
            label.set_visible(False)
        axis.get_yaxis().get_offset_text().set_visible(False)
        axis.set_ylabel("")

def gridPlot1DHistogram(dataarray, title="", invertX=False, invertY=True, shareLabelsX=None, shareLabelsY=None, **kwargs):
    '''Plot a grid of 1d histograms.

    Pass an xarray.dataarray as dataarray with 1 or 2 dimensions. A
    1 or 2d grid will of histograms will be created. Set the title of
    the figure with title.

    To invert the order of histograms on a given axis, use the
    arguments invertX or invertY. The y axis is by default inverted
    to make grid comparable to xarray plots, which have the origin in
    the bottom right.

    If shareLabelsX or shareLabelsY are passed, only the labels of
    the bottom/leftmost subplots are shown (equivalent to
    matplotlib.axes._subplots.label_outer()). If plotRangeX or
    plotRangeY are defined as kwargs, labels are shared automatically.

    All additional kwargs will be passed to histo1d.
    '''
    dims = {}
    gridDims = dataarray.squeeze().dims
    for var in gridDims:
        dims[var] = len(dataarray.coords[var])
    if len(dims) == 1:
        fig, axes = plt.subplots(*([1] + list(dims.values())), figsize=(20, 3.5))
    else:
        fig, axes = plt.subplots(*list(dims.values()), figsize=(20, 13))
    _ = fig.suptitle(title)

    it = np.nditer(axes, flags=['multi_index', 'refs_ok'])
    for _ in it:
        index = invertIndexIterator(it.multi_index, axes, invertX, invertY)
        # Use index to construct dictionary for access,
        # but use multi_index to access subplots. This allows
        # to invert ordering in x and y.
        d = dict(zip(gridDims, index))
        weights = dataarray[d].item()[0]
        bins = dataarray[d].item()[1]
        histo1d(weights, bins, axes=axes[it.multi_index], **kwargs)

        label = [f"{key}={val}" for key, val in d.items()]
        label = [f"{key}={dataarray.squeeze(drop=True).coords[key].data[val]}" for key, val in d.items()]

        ax = axes[it.multi_index]
        ax.title.set_text(", ".join(label))

    xRangeDefined = 'plotRangeX' in kwargs and not kwargs['plotRangeX'] == None
    yRangeDefined = 'plotRangeY' in kwargs and not kwargs['plotRangeY'] == None
    for ax in axes.flat:
        if shareLabelsX or \
           shareLabelsX == None and xRangeDefined:
            label_outer_onlyX(ax)
        if shareLabelsY or \
           shareLabelsY == None and yRangeDefined:
            label_outer_onlyY(ax)

def gridPlot2DHistogram(dataarray, gridDims, title="", colorTitle="", invertX=False, invertY=False):
    '''Plots a grid of 2d histograms with color coding.

    Pass a xarray.dataarray as dataarray with 3 or 4 dimensions.
    Specify the variable names of 1 or 2 dimensions, respectively, as
    gridDims. These dimensions will be shown as grid axes. The other
    dimensions in the plots. title and colorTitle determine the title
    of the figure and the label on the color bar. invertX and invertY
    can be used to invert the order of the elements on the x and y
    axes.
    '''
    dims = {}
    rang = max(abs(min(dataarray.data.flatten())), abs(max(dataarray.data.flatten())))
    for var in gridDims:
        dims[var] = len(dataarray.coords[var])
    if len(dims) == 0:
        log.ERROR("Please indicate in the argument gridDims which dimensions the grid should have. Must have length > 0.")
        return
    if len(dims) == 1:
        fig, axes = plt.subplots(*([1] + list(dims.values())), figsize=(20, 3.5))
    else:
        fig, axes = plt.subplots(*list(dims.values()), figsize=(13, 13))
    _ = fig.suptitle(title)

    it = np.nditer(axes, flags=['multi_index', 'refs_ok'])
    for _ in it:
        index = invertIndexIterator(it.multi_index, axes, invertX, invertY)
        d = dict(zip(gridDims, index))
        im = dataarray[d].squeeze(drop=True).plot(cmap="RdGy", ax=axes[it.multi_index], vmin=-rang, vmax=rang, label=None, add_colorbar=False)

        label = [f"{key}={val}" for key, val in d.items()]
        label = [f"{key}={dataarray.coords[key].data[val]}" for key, val in d.items()]

        ax = axes[it.multi_index]
        ax.title.set_text(", ".join(label))

    for ax in axes.flat:
        ax.label_outer()

    cax = fig.add_axes([0.93, 0.2, 0.03, 0.6])
    _ = fig.colorbar(im, cax=cax, label=colorTitle)


def histo1d_fromDataArray(da, axes=None, xlabel=None, ylabel="Count", **kwargs):
    '''Plot a 1d histogram with weights, bins stored in a DataArray.

    This function is a wrapper around `matplotlib.pyplot.hist()`.

    :param da: DataArray in which the weights and bin edges of a
        histogram are stored.
    :type da: `xarray.DataArray`
    :param axes: Axes to draw histogram on. If `None` are provided,
        they are created automatically.
    :type axes: `matplotlib.axes.Axes`
    :param xlabel: Label for the x axis. If `None`, the name of the
        DataArray is displayed.
    :type xlabel: `str`
    :param ylabel: Label for the y axis. By default, "Count" is
        displayed.
    :type ylabel: `str`
    :param ``**kwargs``: Keyword arguments are forwarded to
        `matplotlib.pyplot.hist()`. The arguments `x`, `bins` and
        `weights` are already used.
    :returns: `False`, if input is not of the expected format.
        Otherwise the same return value that
        `matplotlib.pyplot.hist()` returns.
    '''
    if isinstance(da, xr.core.dataarray.DataArray):
        weights = da.values.item()[0]
        bins = da.values.item()[1]
    elif isinstance(da, np.ndarray):
        if da.shape == ():
            weights = da.item()[0]
            bins = da.item()[1]
        else:
            log.WARNING("histo1d_fromDataArray(): The argument is a numpy.ndarray, but its shape isn't empty. Not sure what to do with it. It's safest to just pass an xarray.DataArray.")
            return False
    elif isinstance(da, tuple):
        if len(da) == 2:
            weights = da[0]
            bins = da[1]
        else:
            log.WARNING("histo1d_fromDataArray(): The argument is a tuple, but its length isn't 2. Not sure what to do with it. It's safest to just pass an xarray.DataArray.")
            return False

    if not axes:
        fig, axes = plt.subplots()

    if ylabel:
        axes.set_ylabel(ylabel)
    if xlabel:
        axes.set_xlabel(xlabel)
    elif isinstance(da, xr.core.dataarray.DataArray):
        axes.set_xlabel(da.name)

    retVal = plt.hist(bins[0:-1], bins=bins, weights=weights, **kwargs)
    return retVal

def FFE(weights, bins, median=None, trueValue=None, mean=None, mean_unc=None, stddev=None, asymStddev=None, asymStddevGreater0=None, expression=None, listOfVariedValues=None, saveAs=None, plotRangeX=None, plotRangeY=None, meanMedianSameLine=True):
    allArgs = locals()

    # First, count the number of lines needed for the top panel. Save
    # all variables in args. If the variable is an xarray DataArray,
    # it is unpacked.
    relevantForTopPanel = ["mean",
                           # "mean_unc", this one doesn't get its own line
                           "median",
                           "trueValue",
                           "stddev",
                           "asymStddev",
                           "asymStddevGreater0",
                           "expression",
                           "listOfVariedValues"]
    args = {}
    rowIndex = 0
    for arg,val in allArgs.items():
        if id(val) == id(None):
            continue
        if isinstance(val, xr.core.dataarray.DataArray) and val.size == 1:
            args[arg] = val.item()
        else:
            args[arg] = val
        if arg not in relevantForTopPanel:
            continue
        rowIndex += 1
    if "median" in args and "mean" in args and meanMedianSameLine:
        rowIndex -= 1
    else:
        meanMedianSameLine = False

    colors = ['tab:blue',
              'tab:orange',
              'tab:green',
              'tab:red',
              'tab:purple',
              'tab:brown',
              'tab:pink',
              'tab:gray',
              'tab:olive',
              'tab:cyan',
              ]

    if rowIndex == 0:
        fig, ax_hist = plt.subplots()
    else:
        ratio = (0.1 + 0.1*rowIndex/8, 0.9 - 0.1*rowIndex/8)
        fig, (ax_top, ax_hist) = plt.subplots(2,
                                              sharex=True,
                                              gridspec_kw={"height_ratios": ratio},
                                              figsize=getDefaultFigsize())

        ax_top.spines['top'].set_visible(False)
        ax_top.spines['right'].set_visible(False)
        ax_top.spines['left'].set_visible(False)
        ax_top.get_yaxis().set_ticks([])
        ax_top.set_ylim(0.0, rowIndex+0.5)
        ax_hist.set_xlabel("Fake factor estimate")
        ax_hist.set_ylabel("Count")

        # plot mean, median and true value
        if "mean" in args:
            if not "mean_unc" in args:
                # Without errorbars
                ax_top.plot(args["mean"], [rowIndex],
                            marker='o',
                            linestyle='',
                            color=colors[2],
                            label="Mean")
            else:
                # With errorbars
                ax_top.errorbar([args["mean"]], [rowIndex],
                                xerr=np.array([[args["mean_unc"]], [args["mean_unc"]]]),
                                fmt='o',
                                color=colors[2],
                                capsize=2.5,
                                capthick=1.5,
                                label="Mean")
        if not meanMedianSameLine:
            rowIndex -= 1

        if "median" in args:
            ax_top.plot([args["median"]], [rowIndex], marker='*', linestyle='', color=colors[1], label="Median")
            rowIndex -= 1

        if "trueValue" in args:
            ax_top.plot([args["trueValue"]] , [rowIndex], marker='*', linestyle='', color=colors[3], label="True value")
            rowIndex -= 1

        if "stddev" in args and "mean" in args:
            ax_top.errorbar([mean], [rowIndex],
                            xerr=np.array([[args["stddev"]], [args["stddev"]]]),
                            fmt='o',
                            color=colors[4],
                            capsize=2.5,
                            capthick=1.5,
                            label="Standard deviation")
            rowIndex -= 1

        if "asymStddev" in args and "mean" in args:
            asymStddevUp, asymStddevDown = asymStddev.item()
            ax_top.errorbar([mean], [rowIndex],
                            xerr=np.array([[asymStddevDown], [asymStddevUp]]),
                            fmt='o',
                            color=colors[5],
                            capsize=2.5,
                            capthick=1.5,
                            label="asym stddev")
            rowIndex -= 1

        if "asymStddevGreater0" in args and "mean" in args:
            asymStddevGreater0Up, asymStddevGreater0Down = asymStddevGreater0.item()
            ax_top.errorbar([mean], [rowIndex],
                            xerr=np.array([[asymStddevGreater0Down], [asymStddevGreater0Up]]),
                            fmt='o',
                            color=colors[6],
                            capsize=2.5,
                            capthick=1.5,
                            label="asym stddev > 0")
            rowIndex -= 1

        # plot variations and expression
        if "expression" in args:
            stddev = args["expression"].std_dev
            ax_top.errorbar([args["expression"].n], [rowIndex],
                            xerr=np.array([[stddev], [stddev]]),
                            fmt='o',
                            color=colors[7],
                            capsize=2.5,
                            capthick=1.5,
                            label="expression")
            rowIndex -= 1
        if "listOfVariedValues" in args and "trueValue" in args:
            varUp, varDown = combineListOfVariedValues(args["listOfVariedValues"], args["trueValue"])
            ax_top.errorbar([args["trueValue"]], [rowIndex],
                            xerr=np.array([[varDown], [varUp]]),
                            fmt='o',
                            color=colors[8],
                            capsize=2.5,
                            capthick=1.5,
                            label="variations")
            rowIndex -= 1

        if not rowIndex == 0:
            log.ERROR(f"row index is {rowIndex} (should be 0)")
        # Legend
        handles, labels = ax_top.get_legend_handles_labels()

    ax_hist.spines['top'].set_visible(False)
    ax_hist.spines['right'].set_visible(False)
    ax_hist.label_outer()

    # Change plot range according to plotRange. First entry is the
    # low limit, second the high limit.
    if isinstance(plotRangeX, tuple):
        ax_hist.set_xlim(*plotRangeX)
    if isinstance(plotRangeY, tuple):
        ax_hist.set_ylim(*plotRangeY)

    # plot histogram
    ax_hist.hist(bins[:-1], bins=bins, weights=weights, color=colors[0])
    ax_hist.grid()

    # # Use exponential notation when axis range > 1e5.
    # # Otherwise y axis label gets cut off.
    # # update: not necessary anymore, since using tight_layout
    # ax_hist.ticklabel_format(axis="y", style='sci', scilimits=(-4, 5))

    if not "handles" in locals():
        handles = []
    if not "labels" in locals():
        labels = []
    # print(str(handles))
    # print(str(labels))
    # print(str(mpatches.Patch(color=colors[0], label='pseudo experiments')))

    # Make sure Mean is at the top of the legend.
    if "Mean" in labels:
        index = labels.index("Mean")
        label = labels.pop(index)
        labels.insert(0, label)
        handle = handles.pop(index)
        handles.insert(0, handle)

    handles.append(mpatches.Patch(color=colors[0]))
    labels.append('Pseudo experiments')

    ax_hist.legend(handles, labels)

    # Rearrange subplot and labels to make them fit better.
    # Removes a lot of empty space from the top.
    fig.tight_layout()

    if not saveAs == None:
        outPath = utils.getOutputPath(saveAs)
        log.INFO(f"Plotting {outPath}")
        plt.savefig(outPath)
