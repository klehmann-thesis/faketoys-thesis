import unittest
import argparse
import pickle
from faketoys import utils, prepare, compute, analyze, plot, submit
from faketoys.config import minimalTest, example
import random
import numpy as np
import os

class TestDistributions(unittest.TestCase):

    def setUp(self):
        return
    def tearDown(self):
        return

    def test_example(self):
        argsPrepare = utils.getArgParserPrepare().parse_args(["config/example.py"])
        argsCompute = utils.getArgParserCompute().parse_args(["config/example.py"])
        prepare.main(argsPrepare)
        compute.main(argsCompute)

        computedFile = utils.getInputPath(example.computedOutputFile)
        with open(computedFile, 'rb') as f:
            ds = pickle.load(f)
        self.assertEqual(ds.h_random.attrs['nRandom'], example.compute_nRandom)

        da = ds["h_random"].loc[{"Gaus_mean": 100, "Pois_mean": 10}]
        plot1 = plot.histo1d_fromDataArray(da, xlabel="x")
        plot2 = plot.histo1d_fromDataArray(da.values, xlabel="x")
        plot3 = plot.histo1d_fromDataArray(da.values.item(), xlabel="x")
        self.assertEqual(plot1.__repr__(), plot2.__repr__())
        self.assertEqual(plot2.__repr__(), plot3.__repr__())

    def test_runChain(self):

        argsPrepare = utils.getArgParserPrepare().parse_args(["config/minimalTest.py"])
        argsCompute = utils.getArgParserCompute().parse_args(["config/minimalTest.py"])
        vars(argsCompute)["random_seed"] = 10
        argsAnalyze = utils.getArgParserAnalyze().parse_args(["config/minimalTest.py"])

        prepare.main(argsPrepare)
        compute.main(argsCompute)
        analyze.main(argsAnalyze)

        computedFile = utils.getInputPath(minimalTest.computedOutputFile)
        with open(computedFile, 'rb') as f:
            ds = pickle.load(f)

        self.assertEqual(ds.h_random.attrs['nRandom'], minimalTest.compute_nRandom)

        # just test that code doesn't crash
        plot.pValue_fromDataset(ds.loc[{"Nl": 16, "Nlr": 8, "Nlr_unc": 4}])
        plot.gridPlot1DHistogram(ds.loc[{"Nl": 16}].h_random,
                                 shareLabelsX=True,
                                 shareLabelsY=True)
        plot.gridPlot2DHistogram(ds["mean"],
                                 ["Nlr"],
                                 invertX=True)

        # rerun compute step parallelized and verify that output is the same
        vars(argsCompute)["parallelize"] = True
        vars(argsCompute)["num_workers"] = 3
        compute.main(argsCompute)
        with open(computedFile, 'rb') as f:
            ds1 = pickle.load(f)
        self.assertTrue(all((ds["mean"].values == ds1["mean"].values).flatten()))

    def test_histogramming(self):
        """Test histogramming (1d and 2d) with under and overflow"""

        # 1d
        entries = [0.2, 0.4, 0.5, 1.5, 2.1, 2.3, 2.5, 2.5, 3.5, 100]
        bins = [-1, 0, 1, 2, 3, 4]

        hist_check = [0, 3, 1, 4, 1]
        hist_underOverflow_check = [0, 3, 1, 4, 2]
        hist, _ = compute.histogram(entries, bins=bins, underOverflowX=False)
        hist_underOverflow, _ = compute.histogram(entries, bins=bins, underOverflowX=True)
        self.assertEqual(list(hist), hist_check)
        self.assertEqual(list(hist_underOverflow), hist_underOverflow_check)

        entries = [-2, -1.5, 0.5, 0.5, 1.5, 2.1, 2.3, 2.5, 2.5, 3.5]
        bins = [-1, 0, 1, 2, 3, 4]
        hist_check = [0, 2, 1, 4, 1]
        hist_underOverflow_check = [2, 2, 1, 4, 1]
        hist, _ = compute.histogram(entries, bins=bins, underOverflowX=False)
        hist_underOverflow, _ = compute.histogram(entries, bins=bins, underOverflowX=True)
        self.assertEqual(list(hist), hist_check)
        self.assertEqual(list(hist_underOverflow), hist_underOverflow_check)

        #2d
        entriesX = [random.uniform(0, 10) for x in range(10)]
        entriesY = [random.uniform(0, 10) for x in range(10)]
        entriesX = entriesX + [-1, -1, 11, 11, 4 ,  6]
        entriesY = entriesY + [-1,  4,  6, 11, -1, 11]
        bins = (np.linspace(0, 10, 11), np.linspace(0, 10, 11))
        binsX = bins[0]
        binsY = bins[1]

        hist, _ = compute.histogram2d(entriesX, entriesY, binsX=binsX, binsY=binsY, underOverflowX=False, underOverflowY=False)
        histRaw, _, _ = np.histogram2d(entriesX, entriesY, bins=bins)
        self.assertTrue(np.array_equal(hist, histRaw))

        underOverflow = np.zeros_like(histRaw)
        underOverflow[4, 0] = 1
        underOverflow[6, 9] = 1
        hist, _ = compute.histogram2d(entriesX, entriesY, binsX=binsX, binsY=binsY, underOverflowX=False, underOverflowY=True)
        self.assertTrue(np.array_equal(hist, histRaw + underOverflow))

        underOverflow = np.zeros_like(histRaw)
        underOverflow[0, 4] = 1
        underOverflow[9, 6] = 1
        hist, _ = compute.histogram2d(entriesX, entriesY, binsX=binsX, binsY=binsY, underOverflowX=True, underOverflowY=False)
        self.assertTrue(np.array_equal(hist, histRaw + underOverflow))

        underOverflow = np.zeros_like(histRaw)
        underOverflow[0, 0] = 1
        underOverflow[0, 4] = 1
        underOverflow[9, 6] = 1
        underOverflow[9, 9] = 1
        underOverflow[4, 0] = 1
        underOverflow[6, 9] = 1
        hist, _ = compute.histogram2d(entriesX, entriesY, binsX=binsX, binsY=binsY, underOverflowX=True, underOverflowY=True)
        self.assertTrue(np.array_equal(hist, histRaw + underOverflow))


    def test_deviationFromUniform(self):
        weights = [x for x in range(1, 10, 1)]
        dev = analyze.getDeviationFromUniform(weights)
        dev_check = np.sqrt(2*(16+9+4+1))
        self.assertEqual(dev, dev_check)
        dev = analyze.getDeviationFromUniform(weights, perBin=True)
        self.assertEqual(dev, dev_check / 9)

        dev_exp = analyze.getExpectedDeviationFromUniform(weights)
        dev_exp_check = np.sqrt(45 * (1-1/9))
        self.assertEqual(dev_exp, dev_exp_check)
        dev_exp = analyze.getExpectedDeviationFromUniform(weights, perBin=True)
        self.assertEqual(dev_exp, dev_exp_check / 9)

        dev = analyze.getDeviationFromUniform(weights, perExp=True)
        self.assertEqual(dev, dev_check / dev_exp_check)
        dev = analyze.getDeviationFromUniform(weights, perBin=True, perExp=True)
        self.assertEqual(dev, dev_check / dev_exp_check)


    def test_croppingArrays(self):
        bins = tuple([[0, 1, 2, 3, 5], [0, 1, 2, 3, 4, 5, 6]])
        weights = np.random.rand(4, 6)
        newWeights, newBins = plot.crop2dArray(weights, bins, plotRangeX=(1, 5), plotRangeY=(0, 5))
        self.assertEqual(newBins[0], [1, 2, 3, 5])
        self.assertEqual(newBins[1], [0, 1, 2, 3, 4, 5])
        np.testing.assert_array_equal(newWeights, weights[1:, :-1])
        print(weights)
        print(weights[1:, :6])

    def test_submit(self):
        '''Basic test of submission script.

        Makes sure that the environment is defined correctly and that
        arguments are understood correctly.'''

        CLI_args = ["config/minimalTest.py", "--time", "120", "--num_workers", "4"]
        argsSubmit = utils.getArgParserSubmit().parse_args(CLI_args)
        self.assertTrue(submit.main(argsSubmit))

    def test_pValueCalculation(self):
        pVal = compute.getPValue

        # Test that 0.5 is returned every time the randomValue equals
        # the trueValue
        self.assertEqual(pVal(400, 400, 10, 10), 0.5)
        self.assertEqual(pVal(400, 400, 0, 10), 0.5)
        self.assertEqual(pVal(400, 400, 10, 0), 0.5)
        self.assertEqual(pVal(400, 400, 0, 0), 0.5)
        self.assertAlmostEqual(pVal(400, 400.001, 10, 1e9), 0.5)
        self.assertAlmostEqual(pVal(400, 399.999, 1e9, 10), 0.5)

        # Test that 0 and 1 are returned if randomValue is extreme or
        # uncertainties are small.
        self.assertAlmostEqual(pVal(400, 0, 1e-9, 100), 1)
        self.assertAlmostEqual(pVal(400, 800, 100, 1e-9), 0)

        # Test that interpolation is well defined. Test for typical
        # deviations of 1, 2, 3 sigma.

        # Probability within 1, 2, 3 sigma around maximum
        withinOneSigma = 0.682689492
        outsideOneSigma = 1-withinOneSigma
        withinTwoSigma = 0.954499736
        outsideTwoSigma = 1-withinTwoSigma
        withinThreeSigma = 0.997300204
        outsideThreeSigma = 1-withinThreeSigma

        self.assertAlmostEqual(pVal(400, 500, 50, 100), outsideOneSigma/2)
        self.assertAlmostEqual(pVal(550, 500, 50, 100), 1-outsideOneSigma/2)
        self.assertAlmostEqual(pVal(300, 500, 50, 100), outsideTwoSigma/2)
        self.assertAlmostEqual(pVal(600, 500, 50, 100), 1-outsideTwoSigma/2)
        self.assertAlmostEqual(pVal(200, 500, 50, 100), outsideThreeSigma/2)
        self.assertAlmostEqual(pVal(650, 500, 50, 100), 1-outsideThreeSigma/2)

    def test_pathResolving(self):
        absPath = "/some/absolute/path"
        self.assertEqual(absPath, utils.resolveConfigPath(absPath))

        self.assertTrue("FAKEBASE" in os.environ)

        faketoys = os.environ["FAKEBASE"] + "/faketoys"
        pathRelToFaketoys = "config/example.py"
        self.assertEqual(faketoys + "/" + pathRelToFaketoys, utils.resolveConfigPath(pathRelToFaketoys))

        cwd = os.getcwd()
        for x in os.listdir():
            absPath = cwd + "/" + x
            if os.path.isfile(absPath):
                break
        self.assertEqual(absPath, utils.resolveConfigPath(x))

if __name__ == '__main__':
    unittest.main()
