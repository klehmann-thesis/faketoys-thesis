import unittest

import numpy as np
from faketoys.stats import dist_base, dists
from faketoys.variations.models import SymmetricLinearVariation, AbsoluteVariation
from faketoys.variations.config import VariationConfig

class TestGaussian(unittest.TestCase):

    def setUp(self):
        self.randomMean = np.random.uniform()
        self.randomStddev = np.random.uniform()
        self.randomName = "notSoRandomName"
        self.randomGaussian = dists.Gaussian(self.randomMean, self.randomStddev, name=self.randomName)
        return
    def tearDown(self):
        dist_base.DistributionBase.smallestUnusedCacheIndex = 0
        return

    def test_basics(self):
        self.assertEqual(self.randomGaussian.getMean(), self.randomMean)
        self.assertEqual(self.randomGaussian.getStddev(), self.randomStddev)
        self.assertTrue(isinstance(self.randomGaussian.__str__(), str))
        self.assertTrue(isinstance(self.randomGaussian.__repr__(), str))

    def test_pdf(self):
        from scipy.stats import norm
        x = np.random.uniform()

        pdf1 = self.randomGaussian.getProbabilityDensity(x)
        normFactor = 1/np.sqrt(2*np.pi*(self.randomStddev**2))
        pdf2 = norm.pdf(x, self.randomMean, self.randomStddev)

        self.assertAlmostEqual(pdf1, pdf2)
        return

    def test_getStddev(self):
        stddev = 10
        mean = 100
        gAbs = dists.Gaussian(mean, stddev, relStddev=False, name="abs Gaussian")
        gRel = dists.Gaussian(mean, stddev, relStddev=True, name="rel Gaussian")

        rAbs = gAbs.getRandom(cacheIndex=200)
        stddevAbs = gAbs.getStddev(cacheIndex=200)
        self.assertEqual(stddev, stddevAbs)

        rRel = gRel.getRandom(cacheIndex=200)
        stddevRel = gRel.getStddev(cacheIndex=200)
        self.assertNotEqual(stddev, stddevRel)
        self.assertEqual(stddevRel, stddev * rRel / mean)

        return

    def test_DistributionBase_getRandom_variationConfigs(self):

        var10 = SymmetricLinearVariation(0.1, name="var10")
        var50 = SymmetricLinearVariation(0.5)

        sys1 = dists.Gaussian(1, 2, name="sysVar1")
        sys2 = dists.Gaussian(1, 2)
        sys2.setName("sysVar2")

        variationConfig1 = VariationConfig(sys1, var10, name="variationConfig1")
        variationConfig2 = VariationConfig(sys2, var50)

        gaussianMean = 20
        g = dists.Gaussian(gaussianMean, 1, name="g")
        self.assertTrue(g.addVariationConfig(variationConfig1))
        self.assertTrue(g.addVariationConfig(variationConfig2))

        self.assertFalse(variationConfig1.checkConsistency())

        # Because of the asymmetric systematics (Gaussian with mean not at 0), a
        # bias is introduced when they are implicitly run over. This is not
        # really nice, but multiple warning messages are printed related to the
        # variationConfig.
        index = 1
        r1 = g.getRandom(cacheIndex=index)
        r2 = g.getRandom(cacheIndex=index, systSources=[])
        self.assertNotEqual(r1, r2)

        # r3 has the same problematic variationConfigs as r2. The result is the
        # same. This test shows that the statUnc can also be specified as
        # systSource.
        r3 = g.getRandom(cacheIndex=index, systSources=[g], statUnc=False)
        self.assertEqual(r2, r3)
        return

    def test_DistributionBase_getRandom_variationConfigs(self):

        var10 = SymmetricLinearVariation(0.1, name="var10"
                                                                      )
        var50 = SymmetricLinearVariation(0.5)

        sys1 = dists.Gaussian(0, 2, name="sysVar1"#, level=self.level
                                     )
        sys2 = dists.Gaussian(0, 2)
        sys2.setName("sysVar2")

        variationConfig1 = VariationConfig(sys1, var10, name="variationConfig1")
        variationConfig2 = VariationConfig(sys2, var50, name="variationConfig2")

        gaussianMean = 20
        g = dists.Gaussian(gaussianMean, 1, name="g"
                                  )
        self.assertTrue(g.addVariationConfig(variationConfig1))
        self.assertTrue(g.addVariationConfig(variationConfig2))

        index = 1
        r1 = g.getRandom(cacheIndex=index)
        r2 = g.getRandom(cacheIndex=index, systSources=[])
        self.assertEqual(r1, r2)

        self.assertEqual(g.getRandom(cacheIndex=index, systSources="wrongArgType"), None)
        r3 = g.getRandom(cacheIndex=index, systSources=[sys1, sys2])
        r4 = g.getRandom(cacheIndex=index, systSources=[sys1, sys2])
        self.assertEqual(r3, r4)
        r5 = g.getRandom(cacheIndex=index, systSources=[sys1])
        diff1 = variationConfig2.drawVariation(cacheIndex=index, nominalValue=g.getRandom(cacheIndex=index))
        diff2 = variationConfig2.drawVariation(cacheIndex=index, nominalValue=g.getRandom(cacheIndex=index), systSources=[sys2])
        diff3 = variationConfig2.drawVariation(cacheIndex=index, nominalValue=g.getRandom(cacheIndex=index), systSources=[sys2])
        self.assertEqual(diff1, diff2)
        self.assertEqual(diff2, diff3)

        self.assertAlmostEqual(r4, r5 + diff1)
        wrongDiff = variationConfig2.drawVariation(cacheIndex=index+1, nominalValue=gaussianMean)
        self.assertNotEqual(r4, r5 + wrongDiff)

        # Now test if different variation configs use the same cached
        # variable. Compare variationConfig1 to variationConfig3 and
        # variationConfig4.
        varM10 = SymmetricLinearVariation(-0.1, name="varM10")
        varM20 = SymmetricLinearVariation(-0.2, name="varM20")
        variationConfig3 = VariationConfig(sys1, varM10)
        variationConfig4 = VariationConfig(sys1, varM20)
        index = 2
        var1 = variationConfig1.drawVariation(cacheIndex=index, nominalValue=gaussianMean)
        var3 = variationConfig3.drawVariation(cacheIndex=index, nominalValue=gaussianMean)
        var4 = variationConfig4.drawVariation(cacheIndex=index, nominalValue=gaussianMean)
        # var1 and var3 have the exact opposite effect
        # var1 and 4 are of opposite sign and different by a factor 2
        self.assertEqual(var1, -var3)
        self.assertEqual(var1 * 2, -var4)

        return

    def test_findAllSources(self):
        g1 = dists.Gaussian(5, 30, name="g1")
        sys1 = dists.Gaussian(0, 2, name="sys1")
        sys2 = dists.Gaussian(0, 2, name="sys2")
        sys3 = dists.Gaussian(0, 2, name="sys3")
        c1 = VariationConfig(sys1, AbsoluteVariation(-1))
        c2 = VariationConfig(sys2, AbsoluteVariation(-1))
        c3 = VariationConfig(sys3, AbsoluteVariation(-1))
        self.assertEqual(g1.findAllSources(), [g1])
        g1.addVariationConfig(c1)
        self.assertEqual(g1.findAllSources(), [g1, sys1])
        sys1.addVariationConfig(c2)
        self.assertEqual(g1.findAllSources(), [g1, sys1, sys2])
        sys2.addVariationConfig(c3)
        self.assertEqual(g1.findAllSources(), [g1, sys1, sys2, sys3])

        return


    def test_getRandom_SystematicVariations(self):
        # More high-level example testing interplay between variations

        gaussianMean1 = 20
        gaussianStddev1 = 1
        gaussianMean2 = 100
        gaussianStddev2 = 50
        g1 = dists.Gaussian(gaussianMean1, gaussianStddev1, name="g1")
        g2 = dists.Gaussian(gaussianMean2, gaussianStddev2, name="g2")

        sys1 = dists.Gaussian(0, 2, name="sys1")
        sys2 = dists.Gaussian(0, 2, name="sys2")

        # Two distributions, two systematic uncertainties.
        # g1 depends on sys1, sys2. g2 depends on sys2.

        varConf_g1_sys1 = VariationConfig(sys1, SymmetricLinearVariation(0.1), name="varConf_g1_sys1"
                                                              )
        varConf_g1_sys2 = VariationConfig(sys2, SymmetricLinearVariation(0.4), name="varConf_g1_sys2"
                                                              )
        varConf_g2_sys2 = VariationConfig(sys2, SymmetricLinearVariation(-0.2))

        self.assertEqual(g1.findAllSources(), [g1])
        g1.addVariationConfig(varConf_g1_sys1)
        self.assertEqual(g1.findAllSources(), [g1, sys1])

        index = 10
        self.assertEqual(g1.getRandom(statUnc=False), gaussianMean1)
        self.assertEqual(g1.getRandom(statUnc=False, systSources=[]), gaussianMean1)
        self.assertEqual(g2.getRandom(statUnc=False, cacheIndex=index), gaussianMean2)

        g1_sys1_1 = g1.getRandom(cacheIndex=index, statUnc=False, systSources=[sys1])

        g1.addVariationConfig(varConf_g1_sys2)
        self.assertEqual(g1.findAllSources(), [g1, sys1, sys2])
        g2.addVariationConfig(varConf_g2_sys2)
        self.assertEqual(g1.findAllSources(), [g1, sys1, sys2])
        self.assertEqual(g2.findAllSources(), [g2, sys2])

        g1_sys1_2 = g1.getRandom(cacheIndex=index, statUnc=False, systSources=[sys1])
        self.assertEqual(g1_sys1_1, g1_sys1_2)

        g1_sys2 = g1.getRandom(cacheIndex=index, statUnc=False, systSources=[sys2])
        g2_sys2 = g2.getRandom(cacheIndex=index, statUnc=False, systSources=[sys2])
        # Need to revert the math done in DistributionBase.getRandom() and SymmetricLinearVariation.calculateVariation()
        self.assertAlmostEqual((g1_sys2 - gaussianMean1 ) / (0.4 * gaussianMean1), (g2_sys2 - gaussianMean2) / (-0.2 * gaussianMean2))

        # Test if systSources add linearly
        g1_sys1 = g1.getRandom(cacheIndex=index, statUnc=False, systSources=[sys1])
        g1_sys12 = g1.getRandom(cacheIndex=index, statUnc=False, systSources=[sys1, sys2])
        self.assertAlmostEqual(g1_sys1 + g1_sys2 - g1.getTrueValue(), g1_sys12)

        # Same with stat unc activated
        g1_stat_sys2 = g1.getRandom(cacheIndex=index, systSources=[sys2])
        g1_stat_sys1 = g1.getRandom(cacheIndex=index, systSources=[sys1])
        g1_stat_sys12 = g1.getRandom(cacheIndex=index, systSources=[sys1, sys2])
        g1_stat = g1.getRandom(cacheIndex=index)
        self.assertAlmostEqual(g1_stat_sys1 + g1_stat_sys2 - g1_stat, g1_stat_sys12)



        # Now, test dependency chains. Let sys1 depend on sys2.

        g1.printVariationTree()
        varConf_sys1_sys2 = VariationConfig(sys2, AbsoluteVariation(-0.5), name='varConf_sys1_sys2'
                                                                )
        sys1.addVariationConfig(varConf_sys1_sys2)
        self.assertEqual(sys1.findAllSources(), [sys1, sys2])
        self.assertEqual(g1.findAllSources(), [g1, sys1, sys2])
        g1.printVariationTree()

        # only with sys1
        g1_noUnc = g1.getRandom(cacheIndex=index, systSources=[], statUnc=False)
        g1_stat = g1.getRandom(cacheIndex=index, systSources=[], statUnc=True)
        g1_sys1 = g1.getRandom(cacheIndex=index, systSources=[sys1], statUnc=False)
        g1_stat_sys1 = g1.getRandom(cacheIndex=index, systSources=[sys1], statUnc=True)
        sys1_stat = sys1.getRandom(cacheIndex=index, statUnc=True)
        g1_sys1 = g1.getRandom(cacheIndex=index, systSources=[sys1], statUnc=False)
        g1_sys1_check = gaussianMean1 * (1 + 0.1 * sys1.getRandom(cacheIndex=index))
        self.assertAlmostEqual(g1_sys1, g1_sys1_check)


        # print(f"g1 without unc   : {g1_noUnc}")
        # print(f"g1 with stat     : {g1_stat}")
        # print(f"g1 with sys1     : {g1_sys1}")
        # print(f"g1 with stat+sys1: {g1_stat_sys1}")
        # print(f"sys1 with stat    : {sys1_stat}")
        # dist_base.DistributionBase.logLevel = 0
        # VariationConfig.logLevel = 0
        # SymmetricLinearVariation.logLevel = 0


        g1_stat_sys1 = g1.getRandom(cacheIndex=index, systSources=[sys1], statUnc=True)
        g1_stat_sys1_check = g1.getRandom(cacheIndex=index) * (1 + 0.1 * sys1.getRandom(cacheIndex=index))
        self.assertAlmostEqual(g1_stat_sys1, g1_stat_sys1_check)

        # only with sys2
        sys1_sys2 = sys1.getRandom(cacheIndex=index, statUnc=False, systSources=[sys2])
        self.assertAlmostEqual(sys1_sys2, -0.5 * sys2.getRandom(cacheIndex=index))
        g1_sys2 = g1.getRandom(cacheIndex=index, systSources=[sys2], statUnc=False)
        g1_sys2_check = gaussianMean1 * (1 + 0.1 * (sys1_sys2) + 0.4 * sys2.getRandom(cacheIndex=index))
        self.assertAlmostEqual(g1_sys2, g1_sys2_check)
        g1_stat_sys2 = g1.getRandom(cacheIndex=index, systSources=[sys2], statUnc=True)
        g1_stat_sys2_check = g1.getRandom(cacheIndex=index) * ( 1 + 0.1 * (sys1_sys2) + 0.4 * sys2.getRandom(cacheIndex=index))
        self.assertAlmostEqual(g1_stat_sys2, g1_stat_sys2_check)

        # with sys1 and sys2
        sys1_stat_sys2 = sys1.getRandom(cacheIndex=index, statUnc=True, systSources=[sys2])
        g1_sys1sys2 = g1.getRandom(cacheIndex=index, systSources=[sys1, sys2], statUnc=False)
        g1_sys1sys2_check = gaussianMean1 + gaussianMean1 * ( 0.1 * (sys1_stat_sys2) + 0.4 * sys2.getRandom(cacheIndex=index))
        self.assertAlmostEqual(g1_sys2, g1_sys2_check)
        g1_stat_sys1sys2 = g1.getRandom(cacheIndex=index, systSources=[sys1, sys2], statUnc=True)
        g1_stat_sys1sys2_check = g1.getRandom(cacheIndex=index) * ( 1 + 0.1 * (sys1_stat_sys2) + 0.4 * sys2.getRandom(cacheIndex=index))
        self.assertAlmostEqual(g1_stat_sys1sys2, g1_stat_sys1sys2_check)


        # test if evaluate method returns the same result
        g1_sys1sys2       = g1.getRandom(cacheIndex=index, systSources=[sys1, sys2], statUnc=False)
        g1_sys1sys2_check = g1.evaluate(dist_base.EvaluationMode.RANDOM, cacheIndex=index, systSources=[sys1, sys2], statUnc=False)
        self.assertEqual(g1_sys1sys2, g1_sys1sys2_check)
        g1_stat_sys1sys2       = g1.getRandom(cacheIndex=index, systSources=[sys1, sys2], statUnc=False)
        g1_stat_sys1sys2_check = g1.evaluate(dist_base.EvaluationMode.RANDOM, cacheIndex=index, systSources=[sys1, sys2], statUnc=False)
        self.assertEqual(g1_stat_sys1sys2, g1_stat_sys1sys2_check)
        return

    def test_getVariations(self):
        gaussianMean1 = 20
        gaussianStddev1 = 0.5
        gaussianMean2 = 100
        gaussianStddev2 = 25

        # Two Gaussians (top-level variables)
        g1 = dists.Gaussian(gaussianMean1, gaussianStddev1, name="g1"
                                   )
        g2 = dists.Gaussian(gaussianMean2, gaussianStddev2, name="g2"
                                   )
        g3 = dists.Gaussian(gaussianMean2, gaussianStddev2)

        # Three Gaussians as systematic variations (g1 and g2 depend on sys1 and sys2)
        sys1Stddev = 1
        sys2Stddev = 0.5
        sys3Stddev = 1
        sys1 = dists.Gaussian(0, sys1Stddev, name="sys1")
        sys2 = dists.Gaussian(0, sys2Stddev, name="sys2")
        sys3 = dists.Gaussian(0, sys3Stddev, name="sys3")

        varConf_g1_sys1 = VariationConfig(sys1, SymmetricLinearVariation(0.1 ))
        varConf_g1_sys2 = VariationConfig(sys2, SymmetricLinearVariation(0.4 ))
        varConf_g2_sys2 = VariationConfig(sys2, SymmetricLinearVariation(-0.2))

        g1.addVariationConfig(varConf_g1_sys1)
        g1.addVariationConfig(varConf_g1_sys2)
        g2.addVariationConfig(varConf_g2_sys2)

        g1.printVariationTree()
        g2.printVariationTree()

        for statUnc in [True, False, None]:
            s = statUnc
            if s == None:
                s = True

            sources = g1.findAllSources()
            sourcesWithoutG1 = sources.copy()
            sourcesWithoutG1.remove(g1)
            g1Dict = g1.getDictOfVariedValues(statUnc=s, systSources=sourcesWithoutG1)
            # below, construct the same output manually
            # without argument, all directly affected systematics are
            # considered, i.e. sys1 and sys2, and statistics
            g1Dict_check = {
                # sys1: trueValue +/- 1 * 0.1 * trueValue
                sys1: [22., 18.],
                # sys2: trueValue +/- 0.5 * 0.4 * trueValue
                sys2: [24., 16.],
                }
            if s == True:
                # statistical variation
                g1Dict_check[g1] =  [20.5, 19.5]
            self.assertEqual(g1Dict, g1Dict_check)

            g1Dict = g1.getDictOfVariedValues(statUnc=s, systSources=sources)
            g1Dict_check_1 = g1Dict_check.copy()
            g1Dict_check_1[g1] =  [20.5, 19.5]
            self.assertEqual(g1Dict, g1Dict_check_1)

            g1Dict = g1.getDictOfVariedValues([sys1, sys2, sys3], statUnc=s)
            # With argument, all systematics and statistical unceratinties
            # are considered. Just extend the test from above.
            g1Dict_check[sys3] = [20., 20.]
            self.assertEqual(g1Dict, g1Dict_check)

            # test the same also with retrieving list (make sure to get order right)
            g1List = g1.getListOfVariedValues([sys1, sys2, sys3], statUnc=s)
            g1List_check = [22., 18., 24., 16., 20., 20.]
            if s:
                stat = [20.5, 19.5]
                g1List_check = stat + g1List_check
            self.assertEqual(g1List, g1List_check)

            g1Dict = g1.getDictOfVariedValues([sys1, sys3], statUnc=s)
            del g1Dict_check[sys2]
            self.assertEqual(g1Dict, g1Dict_check)

            g1Dict = g1.getDictOfVariedValues([sys1], statUnc=s)
            del g1Dict_check[sys3]
            self.assertEqual(g1Dict, g1Dict_check)

            g1Dict = g1.getDictOfVariedValues([sys3], statUnc=s)
            del g1Dict_check[sys1]
            g1Dict_check[sys3] = [20., 20.]
            self.assertEqual(g1Dict, g1Dict_check)

            g2Dict = g2.getDictOfVariedValues([sys2], statUnc=s)
            g2Dict_check = {
                # sys1: trueValue +/- 0.5 * -0.2 * trueValue
                sys2: [90., 110.],
                }
            if s == True:
                # statistical variation
                g2Dict_check[g2] =  [125., 75.]
            self.assertEqual(g2Dict, g2Dict_check)

            g2Dict = g2.getDictOfVariedValues(statUnc=s)
            del g2Dict_check[sys2]
            self.assertEqual(g2Dict, g2Dict_check)

            g3Dict = g3.getDictOfVariedValues(statUnc=s)
            g3Dict_check = {}
            if s == True:
                g3Dict_check[g3] = [125., 75.]
            self.assertEqual(g3Dict, g3Dict_check)

            # test if evaluate method returns the same result
            g1Dict_1 = g1.getDictOfVariedValues([sys1, sys2, sys3], statUnc=s)
            g1Dict_2 = g1.evaluate(dist_base.EvaluationMode.VARIATIONDICT, systSources=[sys1, sys2, sys3], statUnc=s)
            self.assertEqual(g1Dict_1, g1Dict_2)
            g1List_1 = g1.getListOfVariedValues([sys1, sys2, sys3], statUnc=s)
            g1List_2 = g1.evaluate(dist_base.EvaluationMode.VARIATIONLIST, systSources=[sys1, sys2, sys3], statUnc=s)
            self.assertEqual(g1List_1, g1List_2)

        # Now, test dependency chains. Let sys1 depend on sys2.

        g1.printVariationTree()
        varConf_sys1_sys2 = VariationConfig(sys2, AbsoluteVariation(-0.5))
        sys1.addVariationConfig(varConf_sys1_sys2)
        g1.printVariationTree()
        g1Dict = g1.getDictOfVariedValues(systSources=g1.findAllSources())
        g1Dict_check = {
            # statistical variation (as above)
            g1: [20.5, 19.5],
            # sys1: trueValue +/- 1 * 0.1 * trueValue (as above)
            sys1: [20 + 1 * 0.1 * 20,   20 - 1 * 0.1 * 20],
            # sys2: the components from above are still valid
            # but the variation of sys1 must be added
            # trueValue +/- 0.5 * 0.4 * trueValue
            sys2: [
                20  +  0.5 * 0.4 * 20  +  (-0.5 * +0.5) * 0.1 * 20,
                20  -  0.5 * 0.4 * 20  +  (-0.5 * -0.5) * 0.1 * 20,
                ]
            }
        self.assertEqual(g1Dict, g1Dict_check)
        return

    def test_expressions_trueValues(self):
        gaussianMean1 = 20
        gaussianStddev1 = 0.5
        gaussianMean2 = 100
        gaussianStddev2 = 25

        # Two Gaussians (top-level variables)
        g1 = dists.Gaussian(gaussianMean1, gaussianStddev1, name="g1"
                                   )
        g2 = dists.Gaussian(gaussianMean2, gaussianStddev2, name="g2"
                                   )
        g3 = dists.Gaussian(gaussianMean2, gaussianStddev2)

        self.assertEqual(g1.getExpression().nominal_value, 20)
        self.assertEqual(g1.getExpression().std_dev, 0.5)
        self.assertEqual(g1.getExpression(statOnly=False).nominal_value, 20)
        self.assertEqual(g1.getExpression(statOnly=False).std_dev, 0.5)
        self.assertEqual(g1.getExpression(statOnly=True).nominal_value, 20)
        self.assertEqual(g1.getExpression(statOnly=True).std_dev, 0.5)

        # Three Gaussians as systematic variations (g1 and g2 depend on sys1 and sys2)
        sys1Stddev = 2
        sys2Stddev = 1
        sys3Stddev = 2
        sys1 = dists.Gaussian(0, sys1Stddev, name="sys1")
        sys2 = dists.Gaussian(0, sys2Stddev, name="sys2")
        sys3 = dists.Gaussian(0, sys3Stddev, name="sys3")

        varConf_g1_sys1 = VariationConfig(sys1, SymmetricLinearVariation(0.1 ))
        varConf_g1_sys2 = VariationConfig(sys2, SymmetricLinearVariation(0.4 ))
        varConf_g2_sys2 = VariationConfig(sys2, SymmetricLinearVariation(-0.2))

        g1.addVariationConfig(varConf_g1_sys1)
        unc = np.sqrt( (gaussianStddev1)**2 + (0.1*gaussianMean1*sys1Stddev)**2 )
        self.assertEqual(g1.getExpression().nominal_value, 20)
        self.assertEqual(g1.getExpression().std_dev, unc)
        self.assertEqual(g1.getExpression(statOnly=False).nominal_value, gaussianMean1)
        self.assertEqual(g1.getExpression(statOnly=False).std_dev, unc)
        self.assertEqual(g1.getExpression(statOnly=True).nominal_value, gaussianMean1)
        self.assertEqual(g1.getExpression(statOnly=True).std_dev, gaussianStddev1)

        self.assertEqual(g1.getExpression(statOnly=False), g1.evaluate(dist_base.EvaluationMode.EXPRESSION, statOnly=False))
        self.assertEqual(g1.getExpression(statOnly=True),  g1.evaluate(dist_base.EvaluationMode.EXPRESSION, statOnly=True))

        return


if __name__ == '__main__':
    unittest.main()
