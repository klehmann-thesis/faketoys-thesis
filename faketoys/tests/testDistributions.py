import unittest

from faketoys.stats import dist_base, dists
from faketoys.variations.models import SymmetricLinearVariation, AbsoluteVariation
from faketoys.variations.config import VariationConfig
from faketoys import utils
import numpy as np
import uncertainties

class TestDistributions(unittest.TestCase):

    def setUp(self):
        return
    def tearDown(self):
        return

    def test_DistributionBase_cacheIndex(self):
        db = dist_base.DistributionBase
        # Set index to 0 as if it hadn't been used yet
        db.smallestUnusedCacheIndex = 0
        self.assertEqual(db.getSmallestUnusedCacheIndex(), 0)
        db.updateSmallestUnusedCacheIndex(100)
        self.assertEqual(db.getSmallestUnusedCacheIndex(), 101)
        db.updateSmallestUnusedCacheIndex(50)
        self.assertEqual(db.getSmallestUnusedCacheIndex(), 101)
        db.updateSmallestUnusedCacheIndex(101)
        self.assertEqual(db.getSmallestUnusedCacheIndex(), 102)
        return

    def test_variationConfigs(self):

        # g1 is a distribution that depends on a systematic
        # unceratinty g2 according to model variationModel
        g1 = dists.Gaussian(20, 3, name = "distr 1")
        # This is a systematic variation, which g1 depends on
        g2 = dists.Gaussian(1, 2, name = "sys 1")
        g3 = dists.Gaussian(1, 2, name = "sys 2")
        variationModel = SymmetricLinearVariation(0.2)
        variationConfig  = VariationConfig(g2, variationModel)
        variationConfig2 = VariationConfig(g3, variationModel)

        self.assertTrue(g1.addVariationConfig(variationConfig))
        self.assertFalse(g1.removeVariationConfig("wrongArgumentType"))
        self.assertFalse(g1.addVariationConfigs([variationConfig]))
        self.assertTrue(g1.removeVariationConfig(variationConfig))
        self.assertEqual(len(g1.getVariationConfigs()), 0)
        self.assertTrue(g1.addVariationConfigs([variationConfig]))
        self.assertEqual(len(g1.getVariationConfigs()), 1)
        self.assertTrue(g1.addVariationConfig(variationConfig2))
        self.assertEqual(len(g1.getVariationConfigs()), 2)
        return

    def test_variationsOfRandomVariables(self):
        """ Test up and down variations based on random value.

        The idea of this method is to test up/down variations of
        sources of systematic uncertainty. This is not done on the true
        value of the distribution (see tests/testGaussian.py), but on
        the random value.

        Example: from a Poisson distribution with mean 100, a random
        variable of 121 is drawn. The standard deviation of the
        distribution is 10, but the observer will estimate the standard
        deviation from the random value as 11.

        Once a random variable has been drawn, the variations as
        estimated from this variable can be retrieved with the same
        cacheIndex. This is tested here.
        """

        # p is our Poisson distribution. p depends on the Gaussian g.
        p = dists.Poisson(100, "poisson")
        g = dists.Gaussian(0, 1, name="gaussian")

        # g lets p vary up and down by a value of 5 = 0.05*100.
        variationModel = SymmetricLinearVariation(0.05)
        variationConfig = VariationConfig(g, variationModel)
        p.addVariationConfig(variationConfig)

        # Retrieve variations around the true value
        allSources = p.findAllSources()
        d = p.getDictOfVariedValues(allSources)
        d_check = {p: [110, 90], g: [105, 95]}
        self.assertEqual(d, d_check)

        # Now retrieve variations using the random value (stat only)
        i = p.getAndUpdateSmallestUnusedCacheIndex()
        rand = p.getRandom(cacheIndex=i, statUnc=True)
        p.cachedValue = 121
        rand = p.getRandom(cacheIndex=i, statUnc=True)
        d = p.getDictOfVariedValues(systSources=[p], cacheIndex=i)
        d_check = {p: [rand + np.sqrt(rand), rand - np.sqrt(rand)]}
        self.assertEqual(d, d_check)

        # Now retrieve variations using the random value
        i = p.getAndUpdateSmallestUnusedCacheIndex()
        rand = p.getRandom(cacheIndex=i, systSources=allSources)
        p.cachedValue = 121
        g.cachedValue = 1
        rand = p.getRandom(cacheIndex=i, systSources=allSources)
        rand_statOnly = float(p.getRandom(cacheIndex=i, statUnc=True))
        g_statOnly = g.getRandom(cacheIndex=i, statUnc=True)

        d = p.getDictOfVariedValues(allSources, cacheIndex=i)
        d_check = {p: [rand + np.sqrt(rand_statOnly), rand - np.sqrt(rand_statOnly)],
                   g: [rand + 0.05 * rand_statOnly, rand - 0.05 * rand_statOnly]}

        self.assertEqual(d, d_check)

        # Check that quadrature sum and pValue give reasonable results
        listOfVariedValues = p.getListOfVariedValues(cacheIndex=i, systSources=allSources, statUnc=True)
        from faketoys import compute
        sigmaUp, sigmaDown = compute.quadratureSumSeparately(listOfVariedValues, rand)
        sigmaUp_check = np.sqrt((d[p][0]-rand)**2 + (d[g][0]-rand)**2)
        sigmaDown_check = np.sqrt((d[p][1]-rand)**2 + (d[g][1]-rand)**2)
        self.assertEqual(sigmaUp, sigmaUp_check)
        self.assertEqual(sigmaDown, sigmaDown_check)
        self.assertAlmostEqual(compute.getPValue(-1000000, rand, sigmaUp, sigmaDown), 0)
        self.assertAlmostEqual(compute.getPValue(+1000000, rand, sigmaUp, sigmaDown), 1)
        # 1/sqrt(2*pi) * integral(exp(-0.5*x**2), x=-1..+1) from wolframalpha
        intMSigmaToPSigma = 0.6826894921370858971704650912640758449558259334532087819747889004
        pValue = compute.getPValue(rand - sigmaDown, rand, sigmaUp, sigmaDown)
        self.assertAlmostEqual(pValue, (1-intMSigmaToPSigma) / 2)


        # Now run the first test again to make sure that caching does
        # not mess up the case, where on caching is requested.
        d = p.getDictOfVariedValues(allSources)
        d_check = {p: [110, 90], g: [105, 95]}
        self.assertEqual(d, d_check)

        return

    def test_basicDistributionMethods(self):
        trueValue = 3
        instances = {}
        instances['gaussian'] = dists.Gaussian(trueValue, 1)
        instances['poisson'] = dists.Poisson(trueValue)
        instances['delta'] = dists.Delta(trueValue)

        for key in instances:
            instance = instances[key]
            self.assertEqual(instance.getTrueValue(), trueValue)
            self.assertEqual(instance.getTrueValue(), instance.evaluate(dist_base.EvaluationMode.TRUEVALUE))
            if key == 'gaussian' or key == 'poisson' or key == 'delta':
                self.assertEqual(trueValue, instance.getMean())
            random = instance.getRandom()
            if key == 'delta':
                self.assertEqual(random, trueValue)
            self.assertEqual(len(instance.getRandom(nRandom = 3)), 3)
            if key == 'gaussian' or key == 'delta':
                self.assertTrue(isinstance(random, float))
            if key == 'poisson':
                self.assertTrue(isinstance(random, int))

            randomNoStat = instance.getRandom(statUnc = False)
            self.assertEqual(randomNoStat, trueValue)

        return

    def test_settingRNG(self):
        '''Ensure that random number generator works fine when
        initialized with seed.'''
        self.assertTrue(dist_base.DistributionBase.setRNG(seed=20))
        p = dists.Poisson(10)
        g = dists.Gaussian(10, 1)
        rp1_1 = p.getRandom()
        rg1_1  = g.getRandom()
        rp2_1 = p.getRandom()
        rng = np.random.default_rng(20)
        self.assertTrue(dist_base.DistributionBase.setRNG(rng=rng))
        self.assertFalse(dist_base.DistributionBase.setRNG(rng=rng, seed=20))
        rp1_2 = p.getRandom()
        rg1_2 = g.getRandom()
        rp2_2 = p.getRandom()
        self.assertEqual(rp1_1, rp1_2)
        self.assertEqual(rg1_1, rg1_2)
        self.assertEqual(rp2_1, rp2_2)

        self.assertFalse(utils.setRandomSeed(None))
        self.assertTrue(utils.setRandomSeed(20))
        rp1_3 = p.getRandom()
        rg1_3 = g.getRandom()
        rp2_3 = p.getRandom()
        self.assertEqual(rp1_1, rp1_3)
        self.assertEqual(rg1_1, rg1_3)
        self.assertEqual(rp2_1, rp2_3)

        # Test that things don't agree with a different seed
        self.assertTrue(dist_base.DistributionBase.setRNG(seed=10))
        self.assertNotEqual(p.getRandom(), rp1_1)
        self.assertNotEqual(g.getRandom(), rg1_1)
        self.assertNotEqual(p.getRandom(), rp2_1)

        # Test that things don't agree with a random seed
        # (this test could fail in principle, but it's very unlikely...)
        self.assertTrue(dist_base.DistributionBase.setRNG())
        self.assertNotEqual(g.getRandom(), rg1_1)

    def test_passingRNG(self):
        '''Ensure that random number generator works fine when
        passed as an argument.'''
        rng = np.random.default_rng(10)
        p = dists.Poisson(10)
        g = dists.Gaussian(10, 1)
        rp1_1 = p.getRandom(rng=rng)
        rg1_1  = g.getRandom(rng=rng)
        rp2_1 = p.getRandom(rng=rng)
        rng = np.random.default_rng(10)
        rp1_2 = p.getRandom(rng=rng)
        rg1_2  = g.getRandom(rng=rng)
        rp2_2 = p.getRandom(rng=rng)
        self.assertEqual(rp1_1, rp1_2)
        self.assertEqual(rg1_1, rg1_2)
        self.assertEqual(rp2_1, rp2_2)

        rng = np.random.default_rng(11)
        rp1_2 = p.getRandom(rng=rng)
        rg1_2  = g.getRandom(rng=rng)
        rp2_2 = p.getRandom(rng=rng)
        self.assertNotEqual(rp1_1, rp1_2)
        self.assertNotEqual(rg1_1, rg1_2)
        self.assertNotEqual(rp2_1, rp2_2)


    def test_dummy(self):
        d = dists.Dummy()
        self.assertTrue(np.isnan(d.getTrueValue()))
        self.assertTrue(np.isnan(d.getMean()))
        self.assertTrue(np.isnan(d.getStddev()))
        self.assertEqual(d.getVariationsForStatUnc(), [])
        self.assertEqual(d.getRandom(nRandom=3), [np.nan for x in range(3)])
        self.assertTrue(np.isnan(d.getRandom()))
        self.assertEqual(d.getExpression().n, 0)
        self.assertEqual(d.getExpression().s, 0)
        self.assertEqual(d.__repr__(), "Dummy")


    def test_expressions_variedValues_Gaussian(self):
        # This method tests if DistributionBase.getRandom(expression=True) works
        # well and if uncertainty propagation for expression works fine.

        for g1_rel in [True, False]:

            g1 = dists.Gaussian(100, 10, relStddev=g1_rel, name="g1")
            g2 = dists.Gaussian(0, 10, relStddev=False, name="g2")

            factor = 0.1
            varconf = VariationConfig(g2, AbsoluteVariation(factor=factor))
            g1.addVariationConfig(varconf)
            r1Exp = g1.getRandom(expression=True)
            self.assertTrue(isinstance(r1Exp, uncertainties.core.Variable))
            self.assertTrue(r1Exp.s, g1.getStddev())

            # None should be equivalent to [g1]
            for systSources in [None, [g1], [g1, g2]]:

                self.assertEqual(g1.getRandom(systSources=systSources, nRandom=2, expression=True), None)

                r1Exp = g1.getRandom(systSources=systSources, cacheIndex=300, expression=True)
                r1 = g1.getRandom(systSources=systSources, cacheIndex=300, expression=False)
                self.assertEqual(r1, r1Exp.n)

                r1 = g1.getRandom(systSources=systSources, cacheIndex=301, expression=False)
                r1Exp = g1.getRandom(systSources=systSources, cacheIndex=301, expression=True)
                self.assertEqual(r1, r1Exp.n)

                stddev_g1_cache = g1.getStddev(cacheIndex=301)
                stddev_g1 = g1.getStddev()
                stddev_g2 = g2.getStddev()
                if g1_rel:
                    if not r1 == g1.getTrueValue():
                        self.assertNotEqual(r1Exp.s, stddev_g1)
                    if systSources in [None, [g1]]:
                        self.assertAlmostEqual(r1Exp.s, 0.1*r1Exp.n)
                        self.assertAlmostEqual(r1Exp.s, stddev_g1_cache)
                    if systSources in [[g1, g2]]:
                        # use g1.getRandom(cacheIndex=301) here, because we don't want to take uncertainty due to g2 into account
                        self.assertAlmostEqual(r1Exp.s, np.sqrt((0.1*g1.getRandom(cacheIndex=301))**2 + (0.1*stddev_g2)**2))
                        self.assertAlmostEqual(r1Exp.s, np.sqrt(stddev_g1_cache**2 + (0.1*stddev_g2)**2))
                else:
                    if systSources in [None, [g1]]:
                        self.assertEqual(r1Exp.s, stddev_g1)
                    elif systSources in [[g1, g2]]:
                        self.assertAlmostEqual(r1Exp.s, np.sqrt(stddev_g1**2 + (0.1*stddev_g2)**2))


    def test_expressions_variedValues_Poisson(self):
        p1 = dists.Poisson(100)
        r = p1.getRandom(cacheIndex=400)
        stddev = p1.getStddev(cacheIndex=400)
        self.assertEqual(np.sqrt(r), stddev)

        stddev = p1.getStddev()
        self.assertEqual(stddev, 10)

if __name__ == '__main__':
    unittest.main()
