import unittest

import numpy as np
from faketoys.stats import derived_dists, dists, dist_base
from faketoys.variations.config import VariationConfig
from faketoys.variations.models import SymmetricLinearVariation
import math

class TestFakeFactor(unittest.TestCase):

    def setUp(self):
        return
    def tearDown(self):
        return

    def test_sumOfDistributions(self):
        g1 = dists.Gaussian(10, 3, name="g1")
        g2 = dists.Gaussian(10, 4, name="g2")
        d3 = dists.Delta(5, name = "d3")
        sod = derived_dists.SumOfDistributions([g1, g2, d3], name="sum")
        pod = derived_dists.ProductOfDistributions([g1, g2, d3], name="product")

        self.assertEqual(sod.getTrueValue(), 25)
        self.assertEqual(sod.getExpression().nominal_value, 25)
        self.assertEqual(sod.getExpression().std_dev, 5)

        self.assertEqual(pod.getTrueValue(), 500)
        self.assertEqual(pod.getExpression().nominal_value, 500)
        self.assertEqual(pod.getExpression().std_dev, 500*math.sqrt(0.3*0.3 + 0.4*0.4))

        # This compares all elements ignoring the order
        self.assertCountEqual(sod.getListOfVariedValues(), [28, 22, 29, 21])
        self.assertCountEqual(pod.getListOfVariedValues(), [650, 350, 700, 300])

        sys1 = dists.Gaussian(0, 1)
        varConf = VariationConfig(sys1, SymmetricLinearVariation(0.05))
        g1.addVariationConfig(varConf)
        self.assertCountEqual(sod.findAllSources(), [g1, g2, d3, sys1])
        self.assertCountEqual(pod.findAllSources(), [g1, g2, d3, sys1])

        # The method printRandomTree is just run to make sure it doesn't fail
        cacheIndex = dist_base.DistributionBase.getAndUpdateSmallestUnusedCacheIndex()
        sod.printRandomTree()
        sod.getRandom(cacheIndex=cacheIndex)
        sod.printRandomTree(cacheIndex=cacheIndex)

        return

if __name__ == '__main__':
    unittest.main()
