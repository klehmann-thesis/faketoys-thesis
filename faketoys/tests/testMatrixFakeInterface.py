import unittest
from faketoys import FakeFactorEquation
from faketoys import MatrixEquation

class TestMatrixFakeInterface(unittest.TestCase):

    def setUp(self):
        return

    def initMatrixAndCalcFake(self, nLeptons):
        # Randomly initialize the matrix equation, convert it into  a
        # FF equation and calculate the fake yield. Then compare to
        # the generated fake yield of the matrix equation.

        # Wrong argument type
        ffe = FakeFactorEquation.FakeFactorEquation(3)
        self.assertEqual(FakeFactorEquation.FakeFactorEquation.fromMatrixEquation(ffe), None)

        me = MatrixEquation.MatrixEquation(nLeptons)
        me.initializeRandom()
        ffe = FakeFactorEquation.FakeFactorEquation.fromMatrixEquation(me, name = "ffe")
        self.assertTrue(ffe.checkConsistency())
        self.assertTrue(ffe.loadDistributions())

        allTight = ffe.nLeptons*"t"
        allReal  = ffe.nLeptons*"r"
        fakeYield = ffe.getTrueValue()
        tightTerm = ffe.getNEvents("", allTight).getTrueValue()
        EWSubtr   = ffe.getNEvents(allReal, allTight).getTrueValue()

        # Test if the methods agree up to the 5th decimal place
        self.assertAlmostEqual(fakeYield, tightTerm-EWSubtr, places=5)
        # Test if the methods agree with a relative precision of 1e-7
        self.assertAlmostEqual((fakeYield - (tightTerm-EWSubtr))/fakeYield, 0, places=7)

    def initMatrixAndConvertBackAndForth(self, nLeptons):
        me = MatrixEquation.MatrixEquation(nLeptons)
        me.initializeRandom()
        ffe = FakeFactorEquation.FakeFactorEquation.fromMatrixEquation(me)
        self.assertTrue(ffe.checkConsistency())

        me2 = MatrixEquation.MatrixEquation.fromFakeFactorEquation(ffe)

        self.assertTrue(me.equalTo(me2))

        for lepIndex in range(me.nLeptons):
            self.assertAlmostEqual(me.r[lepIndex], me2.r[lepIndex])
            self.assertAlmostEqual(me.f[lepIndex], me2.f[lepIndex])
        for index1 in range(me.dimension):
            self.assertAlmostEqual(me.trueVector[index1].item(), me2.trueVector[index1].item())
            self.assertAlmostEqual(me.measuredVector[index1].item(), me2.measuredVector[index1].item())
            for index2 in range(me.dimension):
                self.assertAlmostEqual(me.matrix[index1, index2].item(), me2.matrix[index1, index2].item())
        return

    def test_matrixToFakeInterface(self):
        # Don't test for more than 5 leptons. Remember, the matrix
        # size grows exponentiall as 2^nLeptons.
        for nLeptons in range(1, 6):
            self.initMatrixAndCalcFake(nLeptons)
        return

    def test_convertBackAndForth(self):
        for nLeptons in range(1, 3):
            self.initMatrixAndConvertBackAndForth(nLeptons)
        return

    def tearDown(self):
        return

if __name__ == '__main__':
    unittest.main()
