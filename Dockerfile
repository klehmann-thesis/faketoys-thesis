# Run locally with
#   docker build -t faketoys .

ARG BASEIMAGE=kolehman/faketoys:base

FROM $BASEIMAGE as base
ENV PYTHONPATH=/code
ENV FAKEBASE=/code
ENV FAKEOUTPUT=/fakeoutput
WORKDIR /code
COPY --chown=docker . /code
RUN mkdir /fakeoutput && \
    chown docker:docker /fakeoutput && \
    chown docker:docker /code
USER docker
